<?php

namespace Common\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Uczelnia\PageBundle\Entity\Page;


/**
 * @ORM\Entity(repositoryClass="Common\UserBundle\Repository\UserRepository")
 * @ORM\Table(name="users")
 * @ORM\HasLifecycleCallbacks
 * 
 * @UniqueEntity(fields={"username"})
 */
class User implements AdvancedUserInterface, \Serializable {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length = 20, unique = true)
     * 
     * @Assert\NotBlank(
     *      groups = {"Registration", "ChangeDetails"}
     * )
     * 
     * @Assert\Length(
     *      min=5,
     *      max=20,
     *      groups = {"Registration", "ChangeDetails"}
     * )
     */
    private $username;

    /**
     * @ORM\Column(type="string", length = 64)
     */
    private $password;
    
    /**
     * @Assert\Length(
     *      min = 8
     * )
     */
    private $plainPassword;
    
    /**
     * @ORM\Column(name="account_non_expired", type="boolean")
     */
    private $accountNonExpired = true;
    
    /**
     * @ORM\Column(name="account_non_locked", type="boolean")
     */
    private $accountNonLocked = true;
    
    /**
     * @ORM\Column(name="credentials_non_expired", type="boolean")
     */
    private $credentialsNonExpired = true;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled = false;
    
    /**
     * @ORM\Column(type="array")
     */
    private $roles;
    
    /**
     * @ORM\Column(name="action_token", type="string", length = 20, nullable = true)
     */
    private $actionToken;
    
    /**
     * @ORM\Column(name="register_date", type="datetime")
     */
    private $registerDate;

    /**
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $updateDate;

    /**
     * @ORM\ManyToMany(
     *     targetEntity = "Uczelnia\PageBundle\Entity\Page",
     *     inversedBy = "user"
     * )
     *
     * @ORM\JoinColumn(
     *      name = "page_id",
     *      referencedColumnName = "id",
     *      nullable = true,
     *      onDelete = "CASCADE"
     * )
     */
    private $pages;

    /**
     * @ORM\ManyToMany(
     *     targetEntity = "Uczelnia\PageBundle\Entity\City",
     *     inversedBy = "user"
     * )
     *
     * @ORM\JoinColumn(
     *      name = "city_id",
     *      referencedColumnName = "id",
     *      nullable = true,
     *      onDelete = "CASCADE"
     * )
     */
    private $cities;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "UserPassword",
     *      mappedBy = "user"
     * )
     */
    private $userPasswords;

    public function eraseCredentials() {
        $this->plainPassword = null;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getRoles() {
        if(empty($this->roles)){
            return array('ROLE_USER');
        }
        
        return $this->roles;
    }

    public function getSalt() {
        return null;
    }

    public function getUsername() {
        return $this->username;
    }

    public function isAccountNonExpired() {
        $nowMinus30Days = new \DateTime();
        $nowMinus30Days->sub(date_interval_create_from_date_string('30 days'));
        if ($this->isGranted('ROLE_EDITOR') && $this->getUpdateDate() < $nowMinus30Days) {
            return false;
        }
        return $this->accountNonExpired;
    }

    public function isAccountNonLocked() {
        return $this->accountNonLocked;
    }

    public function isCredentialsNonExpired() {
        return $this->credentialsNonExpired;
    }

    public function isEnabled() {
        return $this->enabled;
    }

//put your code here

    function __construct() {
        $this->registerDate = new \DateTime();
    }

    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set accountNonExpired
     *
     * @param boolean $accountNonExpired
     * @return User
     */
    public function setAccountNonExpired($accountNonExpired)
    {
        $this->accountNonExpired = $accountNonExpired;

        return $this;
    }

    /**
     * Get accountNonExpired
     *
     * @return boolean 
     */
    public function getAccountNonExpired()
    {
        return $this->accountNonExpired;
    }

    /**
     * Set accountNonLocked
     *
     * @param boolean $accountNonLocked
     * @return User
     */
    public function setAccountNonLocked($accountNonLocked)
    {
        $this->accountNonLocked = $accountNonLocked;

        return $this;
    }

    /**
     * Get accountNonLocked
     *
     * @return boolean 
     */
    public function getAccountNonLocked()
    {
        return $this->accountNonLocked;
    }

    /**
     * Set credentialsNonExpired
     *
     * @param boolean $credentialsNonExpired
     * @return User
     */
    public function setCredentialsNonExpired($credentialsNonExpired)
    {
        $this->credentialsNonExpired = $credentialsNonExpired;

        return $this;
    }

    /**
     * Get credentialsNonExpired
     *
     * @return boolean 
     */
    public function getCredentialsNonExpired()
    {
        return $this->credentialsNonExpired;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return User
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set roles
     *
     * @param array $roles
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Set actionToken
     *
     * @param string $actionToken
     * @return User
     */
    public function setActionToken($actionToken)
    {
        $this->actionToken = $actionToken;

        return $this;
    }

    /**
     * Get actionToken
     *
     * @return string 
     */
    public function getActionToken()
    {
        return $this->actionToken;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     * @return User
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime 
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    public function serialize() {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password
        ));
    }

    public function unserialize($serialized) {
        list(
            $this->id,
            $this->username,
            $this->password
        ) = unserialize($serialized);
    }

    
    public function getPlainPassword() {
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword) {
        $this->plainPassword = $plainPassword;
    }

    public function setAccountExpired($accountNonExpired)
    {
        $this->accountNonExpired = !$accountNonExpired;

        return $this;
    }

    public function getAccountExpired()
    {
        return !$this->accountNonExpired;
    }

    public function setAccountLocked($accountNonLocked)
    {
        $this->accountNonLocked = !$accountNonLocked;

        return $this;
    }

    public function getAccountLocked()
    {
        return !$this->accountNonLocked;
    }

    public function setCredentialsExpired($credentialsNonExpired)
    {
        $this->credentialsNonExpired = !$credentialsNonExpired;

        return $this;
    }

    public function getCredentialsExpired()
    {
        return !$this->credentialsNonExpired;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     *
     * @return User
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Add page
     *
     * @param \Uczelnia\PageBundle\Entity\Page $page
     *
     * @return User
     */
    public function addPage(\Uczelnia\PageBundle\Entity\Page $page)
    {
        $this->pages[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Uczelnia\PageBundle\Entity\Page $page
     */
    public function removePage(\Uczelnia\PageBundle\Entity\Page $page)
    {
        $this->pages->removeElement($page);
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * Add city
     *
     * @param \Uczelnia\PageBundle\Entity\City $city
     *
     * @return User
     */
    public function addCity(\Uczelnia\PageBundle\Entity\City $city)
    {
        $this->cities[] = $city;

        return $this;
    }

    /**
     * Remove city
     *
     * @param \Uczelnia\PageBundle\Entity\City $city
     */
    public function removeCity(\Uczelnia\PageBundle\Entity\City $city)
    {
        $this->cities->removeElement($city);
    }

    /**
     * Get cities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCities()
    {
        return $this->cities;
    }

    public function isGranted($role)
    {
        return in_array($role, $this->getRoles());
    }

    /**
     * Add userPassword
     *
     * @param \Common\UserBundle\Entity\UserPassword $userPassword
     *
     * @return User
     */
    public function addUserPassword(\Common\UserBundle\Entity\UserPassword $userPassword)
    {
        $this->userPasswords[] = $userPassword;

        return $this;
    }

    /**
     * Remove userPassword
     *
     * @param \Common\UserBundle\Entity\UserPassword $userPassword
     */
    public function removeUserPassword(\Common\UserBundle\Entity\UserPassword $userPassword)
    {
        $this->userPasswords->removeElement($userPassword);
    }

    /**
     * Get userPasswords
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserPasswords()
    {
        return $this->userPasswords;
    }

    public function hasPasswordInHistory($plainPass, $encoder)
    {
        if ($encoder->isPasswordValid($this, $plainPass)) {
            return true;
        }
        $currentPass = $this->getPassword();
        foreach ($this->getUserPasswords() as $pass) {
            $this->setPassword($pass->getPassword());
            if ($encoder->isPasswordValid($this, $plainPass)) {
                $this->setPassword($currentPass);
                return true;
            }
        }
        $this->setPassword($currentPass);
        return false;
    }
}
