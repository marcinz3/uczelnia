<?php

namespace Common\UserBundle\Controller;

use Common\UserBundle\Entity\UserPassword;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\Exception\CredentialsExpiredException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use Common\UserBundle\Exception\UserException;
use Common\UserBundle\Entity\User;
use Common\UserBundle\Form\Type\LoginType;
use Common\UserBundle\Form\Type\RememberPasswordType;
use Common\UserBundle\Form\Type\RegisterUserType;
use Symfony\Component\Serializer\Exception\Exception;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;


class LoginController extends Controller
{
    /**
     * @Route(
     *      "/logowanie",
     *      name = "blog_login"
     * )
     * 
     * @Template()
     */
    public function loginAction(Request $Request)
    {
        $Session = $this->get('session');
        
        // Login Form
        if($Request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)){
            $loginError = $Request->attributes->get(SecurityContextInterface::AUTHENTICATION_ERROR);
        }else{
            $loginError = $Session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
        }
        
        if(isset($loginError)) {
            //return new Response($loginError);
            if ($loginError instanceof AccountExpiredException) {
                // display the change password form
                $tokenAction = substr(md5(uniqid(NULL, TRUE)), 0, 20);
                $user = $this->getDoctrine()->getRepository('CommonUserBundle:User')
                    ->findOneByUsername($Session->get(SecurityContextInterface::LAST_USERNAME));

                if(null === $user){
                    throw new UserException('Nie znaleziono takiego użytkownika.');
                }

                $user->setActionToken($tokenAction);

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $this->get('session')->getFlashBag()->add('error', 'Twoje hasło wygasło. Musisz je zmienić!');
                return $this->redirect($this->generateUrl('user_changePassword', array('actionToken' => $tokenAction)));
            } else {
                $this->get('session')->getFlashBag()->add('error', $loginError->getMessage());
            }
        }
        
        $loginForm = $this->createForm(new LoginType(), array(
            'username' => $Session->get(SecurityContextInterface::LAST_USERNAME)
        ));

        return array(
            'loginForm' => $loginForm->createView()
        );
    }
    
    
    /**
     * @Route(
     *      "/zmien-haslo/{actionToken}",
     *      name = "user_changePassword"
     * )
     *
     * @Template
     */
    public function changePasswordAction(Request $Request, $actionToken)
    {
        try {
            $form = $this->createFormBuilder()
                ->add('oldPassword', 'password', array(
                    'label' => 'Obecne hasło'
                ))
                ->add('newPassword', 'repeated', array(
                    'type' => 'password',
                    'first_options' => array(
                        'label' => 'Nowe hasło'
                    ),
                    'second_options' => array(
                        'label' => 'Powtórz nowe hasło'
                    ),
                    'required' => true,
                    'invalid_message' => 'Podane hasła nie są takie same',
                    'constraints' => array(
                        new NotBlank(),
                        new Length(array(
                            'min' => 8
                        ))
                    )
                ))
                ->add('save', 'submit', array(
                    'label' => 'Zapisz'
                ))
                ->getForm();

            $form->handleRequest($Request);
            if ($form->isValid()) {
                $formData = $form->getData();
                $user = $this->getDoctrine()->getRepository('CommonUserBundle:User')
                    ->findOneByActionToken($actionToken);

                if (null === $user) {
                    throw new UserException('Podano błędne parametry akcji.');
                }

                $encoder = $this->container->get('security.password_encoder');
                if (!$encoder->isPasswordValid($user, $formData['oldPassword'])) {
                    throw new UserException('Błędne obecne hasło.');
                }

                $newPlainPasswd = $formData['newPassword'];
                if ($user->hasPasswordInHistory($newPlainPasswd, $encoder)) {
                    throw new UserException('Tego hasła już kiedyś używałeś! Wymyśl nowe.');
                }

                $encoderPassword = $encoder->encodePassword($user, $newPlainPasswd);
                $user->setPassword($encoderPassword);
                $user->setActionToken(null);
                $user->setAccountNonExpired(true);
                $user->setUpdateDate(new \DateTime());

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);

                $userPassword = new UserPassword();
                $userPassword->setUser($user)->setPassword($encoderPassword);
                $em->persist($userPassword);
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', 'Hasło zostało zmienione! Możesz się zalogować.');
                return $this->redirect($this->generateUrl('blog_login'));
            }
        } catch (UserException $ex) {
            $this->get('session')->getFlashBag()->add('error', $ex->getMessage());
        }

        //return new Response('greet5yhrthtyh6r');
        return array('form' => $form->createView());
    }
}
