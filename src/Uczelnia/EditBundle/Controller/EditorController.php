<?php

namespace Uczelnia\EditBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class EditorController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $form = $this->createFormBuilder()
            ->add('content', 'ckeditor', array('inline' => true))
            ->getForm();
        return $this->render('UczelniaEditBundle:Editor:index.html.twig', array(
            'title' => '',
            'city' => 'warszawa',
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/{city}/{slug}", name="editor_page")
     */
    public function editorPageAction(Request $Request, $city, $slug)
    {
        $cityRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:City');
        $cityName = $cityRepo->getNameBySlug($city);

        $pageRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:Page');
        $page = $pageRepo->getPage($slug);

        $pageContentRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:PageContent');
        $pageContent = $pageContentRepo->getPageContent($slug, $city);

        if (null === $pageContent) {
            throw $this->createNotFoundException('Strona nie została odnaleziona.');
        }

        $user = $this->getUser();
        if (!$this->get('security.context')->isGranted('ROLE_ADMIN') && (!$user->getCities()->contains($pageContent->getCity()) || !$user->getPages()->contains($page))) {
            throw $this->createAccessDeniedException('Brak uprawnień');
        }

        $form = $this->createFormBuilder()
            ->add('content', 'ckeditor', array(
                'inline' => true,
                'data' => $pageContent->getContent()
            ))
            ->getForm();

        $form->handleRequest($Request);
        if ($form->isValid()) {
            $formData = $form->getData();
            $em = $this->getDoctrine()->getManager();

            $pageContent->setContent($formData['content']);
            $em->persist($pageContent);
            $em->flush();

            $logsRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:Log');
            $user = $this->get('security.context')->getToken()->getUser();
            $logsRepo->addLog($user, 'Zmieniono zawartość strony '.$page->getTitle().' w '.$cityName);

            $this->get('session')->getFlashBag()->add('success', 'Zmiany zostały zapisane');
        }

        return $this->render('UczelniaEditBundle:Editor:index.html.twig', array(
            'title' => $page->getTitle(),
            'pageId' => $page->getId(),
            'city' => $cityName,
            'form' => $form->createView()
        ));
    }
}
