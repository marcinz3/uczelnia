<?php

namespace Uczelnia\AdminBundle\Form\Type;

use Uczelnia\AdminBundle\Controller\PagesController;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ManageUserType extends AbstractType
{
    private $pageRepo;

    public function __construct($pageRepo) {
        $this->pageRepo = $pageRepo;
    }
    
    public function getName()
    {
        return 'manageUser';
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', array(
                'label' => 'Login',
                'attr' => array(
                    'placeholder' => 'nazwa użytkownika'
                )
            ))
            ->add('accountExpired', 'checkbox', array(
                'label' => 'Hasło wygasło',
                'required' => false,
            ))
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'first_options' => array(
                    'label' => 'Hasło'
                ),
                'second_options' => array(
                    'label' => 'Powtórz hasło'
                ),
                'invalid_message' => 'Podane hasła nie są takie same'
            ))
            ->add('pages', 'entity', array(
                'label' => 'Rodzic',
                'multiple' => true,
                'expanded' => true,
                'class' => 'Uczelnia\PageBundle\Entity\Page',
                'property' => 'titleWithPrefix',
                'choices' => PagesController::getPagesHierarchicalList($this->pageRepo),
                'required' => false,
                'label_attr' => array(
                    'class' => 'checkbox-label'
                )
            ))
            ->add('cities', 'entity', array(
                'label' => 'Miasto',
                'multiple' => true,
                'expanded' => true,
                'class' => 'Uczelnia\PageBundle\Entity\City',
                'property' => 'name',
                'label_attr' => array(
                    'class' => 'checkbox-inline'
                )
            ))
            ->add('save', 'submit', array(
                'label' => 'Zapisz'
            ));
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Common\UserBundle\Entity\User'
        ));
    }

    
}