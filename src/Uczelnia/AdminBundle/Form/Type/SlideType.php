<?php

namespace Uczelnia\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Uczelnia\PageBundle\Entity\Page;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SlideType extends AbstractType
{
    public function getName()
    {
        return 'slide';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('slideImg', 'elfinder', array(
                'label' => 'Obraz',
                'instance'=>'form',
                'enable'=> true,
                'attr' => array(
                    'placeholder' => 'Kliknij, aby wybrać (zalecane 1140x425)'
                ),
                'required' => false
            ))
            ->add('slideOrder', 'integer', array(
                'label' => 'Kolejność',
                'scale' => 0,
                'required' => false
            ))
            ->add('save', 'submit', array(
                'label' => 'Zapisz'
            ));
    }


    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Uczelnia\PageBundle\Entity\Slide'
        ));
    }
}