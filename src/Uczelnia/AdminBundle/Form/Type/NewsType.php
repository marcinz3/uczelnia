<?php

namespace Uczelnia\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Uczelnia\PageBundle\Entity\Page;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NewsType extends AbstractType
{
    public function getName()
    {
        return 'news';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'label' => 'Tytuł',
                'attr' => array(
                    'placeholder' => 'Tytuł'
                )
            ))
            ->add('updateSlug', 'checkbox', array(
                'label' => 'aktualizuj adres URL'
            ))
            ->add('content', 'ckeditor', array(
                'label' => 'Treść'
            ))
            ->add('thumbnail', 'elfinder', array(
                'label' => 'Miniaturka',
                'instance'=>'form',
                'enable'=> true,
                'attr' => array(
                    'placeholder' => 'Kliknij, aby wybrać (opcjonalne)'
                ),
                'required' => false
            ))
            ->add('cities', 'entity', array(
                'label' => 'Miasto',
                'multiple' => true,
                'expanded' => true,
                'class' => 'Uczelnia\PageBundle\Entity\City',
                'property' => 'name',
                'label_attr' => array(
                    'class' => 'checkbox-inline'
                )
            ))
            ->add('save', 'submit', array(
                'label' => 'Zapisz'
            ));
    }


    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Uczelnia\PageBundle\Entity\News'
        ));
    }
}