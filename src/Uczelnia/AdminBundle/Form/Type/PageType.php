<?php

namespace Uczelnia\AdminBundle\Form\Type;

use Uczelnia\AdminBundle\Controller\PagesController;
use Symfony\Component\Form\AbstractType;
use Uczelnia\PageBundle\Entity\Page;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PageType extends AbstractType
{
    private $pageRepo;

    public function __construct($pageRepo) {
        $this->pageRepo = $pageRepo;
    }

    public function getName()
    {
        return 'page';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'label' => 'Tytuł',
                'attr' => array(
                    'placeholder' => 'Tytuł'
                )
            ))
            ->add('updateSlug', 'checkbox', array(
                'label' => 'aktualizuj adres URL'
            ))
            ->add('content', 'ckeditor', array(
                'label' => 'Treść'
            ))
            ->add('cities', 'entity', array(
                'label' => 'Miasto',
                'multiple' => true,
                'expanded' => true,
                'class' => 'Uczelnia\PageBundle\Entity\City',
                'property' => 'name',
                'label_attr' => array(
                    'class' => 'checkbox-inline'
                )
            ))
            ->add('parentPage', 'entity', array(
                'label' => 'Rodzic',
                'class' => 'Uczelnia\PageBundle\Entity\Page',
                'property' => 'titleWithPrefix',
                'choices' => PagesController::getPagesHierarchicalList($this->pageRepo),
                'empty_value' => '--',
                'required' => false
            ))
            ->add('menuOrder', 'integer', array(
                'label' => 'Pozycja menu',
                'scale' => 0,
                'required' => false
            ))
            ->add('save', 'submit', array(
                'label' => 'Zapisz'
            ));
    }


    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Uczelnia\PageBundle\Entity\Page'
        ));
    }
}