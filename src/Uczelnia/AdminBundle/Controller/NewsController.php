<?php

namespace Uczelnia\AdminBundle\Controller;

use FM\ElfinderBundle\Form\Type\ElFinderType;
use GuzzleHttp\Psr7\Response;
use Intervention\Image\Exception\NotFoundException;
use Uczelnia\AdminBundle\Form\Type\NewsType;
use Uczelnia\PageBundle\Entity\News;
use Uczelnia\PageBundle\Entity\Page;
use Uczelnia\PageBundle\Entity\PageContent;
use Uczelnia\PageBundle\Repository\PageContentRepository;
use Uczelnia\AdminBundle\Form\Type\PageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class NewsController extends Controller
{
    protected $itemsLimit = 10;

    /**
     * @Route("/aktualnosci/{page}",
     *     name="admin_news_archive",
     *      requirements={"page"="\d+"},
     *      defaults={"page"=1}
     * )
     */
    public function indexAction($page)
    {
        $newsRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:News');
        $qb = $newsRepo->getQueryBuilder();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($qb, $page, $this->itemsLimit);

        $csrfProvider = $this->get('form.csrf_provider');

        return $this->render('UczelniaAdminBundle:News:index.html.twig', array(
            'pagination' => $pagination,
            'csrfProvider' => $csrfProvider,
            'tokenName' => 'delNews%d'
        ));
    }

    /**
     * @Route(
     *      "/aktualnosci/form/{id}/{city}",
     *      name="admin_newsForm",
     *      requirements={"id"="\d+"},
     *      defaults={"id"=NULL, "city"=null}
     * )
     *
     * @Template()
     */
    public function formAction(Request $Request, News $news = null, $city = null)
    {
        if (null == $news) {
            $news = new News();
            $newNewsForm = TRUE;
        }

        $form = $this->createForm(new NewsType(), $news);

        $form->handleRequest($Request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $news->saveForm();
            $em->persist($news);
            $em->flush();

            $message = (isset($newNewsForm)) ? 'Poprawnie dodano nowy news!' : 'News został zmieniony!';
            $this->get('session')->getFlashBag()->add('success', $message);

            return $this->redirect($this->generateUrl('admin_newsForm', array(
                'id' => $news->getId()
            )));
        }

        return array(
            'currPage' => 'pages',
            'form' => $form->createView(),
            'news' => $news
        );
    }

    /**
     * @Route(
     *      "/aktualnosci/delete/{newsId}/{token}",
     *      name="admin_newsDelete",
     *      requirements={"id"="\d+"}
     * )
     */
    public function deleteAction($newsId, $token)
    {
        if (!$this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException('Nie masz uprawnień do tego zadania!');
        }

        $validToken = sprintf('delNews%d', $newsId);
        if (!$this->get('form.csrf_provider')->isCsrfTokenValid($validToken, $token)) {
            throw $this->createAccessDeniedException('Błędy token akcji.');
        }

        $news = $this->getDoctrine()->getRepository('UczelniaPageBundle:News')->find($newsId);
        if ($news == null) {
            throw new NotFoundException('Nie ma takiej strony!');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($news);
        $em->flush();

        return new \Symfony\Component\HttpFoundation\JsonResponse(array(
            'status' => 'ok',
            'message' => 'News został usunięty'
        ));
    }

}
