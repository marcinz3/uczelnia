<?php

namespace Uczelnia\AdminBundle\Controller;

use FM\ElfinderBundle\Form\Type\ElFinderType;
use GuzzleHttp\Psr7\Response;
use Intervention\Image\Exception\NotFoundException;
use Uczelnia\AdminBundle\Form\Type\NewsType;
use Uczelnia\PageBundle\Entity\City;
use Uczelnia\PageBundle\Entity\News;
use Uczelnia\PageBundle\Entity\Page;
use Uczelnia\PageBundle\Entity\PageContent;
use Uczelnia\PageBundle\Repository\PageContentRepository;
use Uczelnia\AdminBundle\Form\Type\PageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;

class CitiesController extends Controller
{
    /**
     * @Route("/miasta",
     *     name="admin_cities_archive"
     * )
     */
    public function indexAction(Request $Request, City $city = null)
    {
        if (null == $city) {
            $city = new City();
        }

        $citiesRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:City');
        $cities = array();
        foreach ($citiesRepo->findAll() as $city) {
            $cities[$city->getSlug()] = $city;
        }

        $forms = array();
        $formViews = array();
        foreach ($cities as $city) {
            $form = $this->createFormBuilder()
                ->add('color', 'text', array(
                    'label' => 'Kolor',
                    'data' => $city->getColor(),
                    'constraints' => array(
                        new NotBlank(),
                        new Length(array('min' => 3, 'max' => 6))
                    )
                ))
                ->add('fb', 'url', array(
                    'label' => 'Facebook',
                    'data' => $city->getFb(),
                    'required' => false,
                    'constraints' => array(
                        new Url(array(
                            'protocols' => array('http', 'https')
                        ))
                    )
                ))
                ->add('twitter', 'url', array(
                    'label' => 'Twitter',
                    'data' => $city->getTwitter(),
                    'required' => false,
                    'constraints' => array(
                        new Url(array(
                            'protocols' => array('http', 'https')
                        ))
                    )
                ))
                ->add('googlePlus', 'url', array(
                    'label' => 'Google+',
                    'data' => $city->getGooglePlus(),
                    'required' => false,
                    'constraints' => array(
                        new Url(array(
                            'protocols' => array('http', 'https')
                        ))
                    )
                ))
                ->add('citySlug', 'hidden', array(
                    'data' => $city->getSlug()
                ))
                ->add('save', 'submit', array(
                    'label' => 'Zapisz'
                ))->getForm();
            $forms[] = $form;
            $formViews[$city->getSlug()]['formView'] = $form->createView();
            $formViews[$city->getSlug()]['city'] = $city;
        }

        $form->handleRequest($Request);
        if ($form->isValid()) {
            $formData = $form->getData();
            $em = $this->getDoctrine()->getManager();

            if (isset($cities[$formData['citySlug']])) {
                $city = $cities[$formData['citySlug']];
                $whatChange = '';

                if ($city->getColor() !== $formData['color']) {
                    $whatChange .= "kolor [{$city->getColor()} -> {$formData['color']}] <br>";
                    $city->setColor($formData['color']);
                }

                if ($city->getFb() !== $formData['fb']) {
                    $whatChange .= "Facebook [{$city->getFb()} -> {$formData['fb']}] <br>";
                    $city->setFb($formData['fb']);
                }

                if ($city->getGooglePlus() !== $formData['googlePlus']) {
                    $whatChange .= "Google+ [{$city->getGooglePlus()} -> {$formData['googlePlus']}] <br>";
                    $city->setGooglePlus($formData['googlePlus']);
                }

                if ($city->getTwitter() !== $formData['twitter']) {
                    $whatChange .= "Twitter [{$city->getTwitter()} -> {$formData['twitter']}] <br>";
                    $city->setTwitter($formData['twitter']);
                }

                $em->persist($city);
                $em->flush();

                $logsRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:Log');
                $user = $this->get('security.context')->getToken()->getUser();
                $logsRepo->addLog($user, 'Zmieniono '.$city->getName().': '.$whatChange);

                $this->get('session')->getFlashBag()->add('success', 'Poprawnie zmieniono miasto: ' . $city->getName());

                return $this->redirect($this->generateUrl('admin_cities_archive'));
            } else {
                $this->get('session')->getFlashBag()->add('error', 'Nie ma takiego miasta: ' . $formData['citySlug']);
            }

        }

        return $this->render('UczelniaAdminBundle:Cities:index.html.twig', array(
            'forms' => $formViews
        ));
    }

}
