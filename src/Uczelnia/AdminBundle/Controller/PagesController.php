<?php

namespace Uczelnia\AdminBundle\Controller;

use FM\ElfinderBundle\Form\Type\ElFinderType;
use GuzzleHttp\Psr7\Response;
use Intervention\Image\Exception\NotFoundException;
use Uczelnia\PageBundle\Entity\Page;
use Uczelnia\PageBundle\Entity\PageContent;
use Uczelnia\PageBundle\Repository\PageContentRepository;
use Uczelnia\AdminBundle\Form\Type\PageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Uczelnia\PageBundle\Repository\PageRepository;

class PagesController extends Controller
{
    /**
     * @Route("/strony", name="pages_archive")
     */
    public function indexAction()
    {
        $pageRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:Page');
        $pages = $this->getPagesHierarchicalList(
            $pageRepo,
            '<span style="color: black" class="glyphicon glyphicon-minus"></span>')
        ;

        return $this->render('UczelniaAdminBundle:Pages:index.html.twig', array(
            'pagesList' => $pages
        ));
    }

    /**
     * @Route(
     *      "/form/{id}/{city}",
     *      name="admin_pageForm",
     *      requirements={"id"="\d+"},
     *      defaults={"id"=NULL, "city"=null}
     * )
     *
     * @Template()
     */
    public function formAction(Request $Request, Page $page = null, $city = null)
    {
        if (null == $page) {
            $page = new Page();
            $newPageForm = TRUE;
        } else if ($city !== null) {
            //$page->setCities($city);
            $pContentRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:PageContent');
            $pageContent = $pContentRepo->getPageContent($page->getSlug(), $city);
            $page->setContent($pageContent->getContent());
            $page->setCities(new \Doctrine\Common\Collections\ArrayCollection(array($pageContent->getCity())));
        }

        $pageRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:Page');
        $form = $this->createForm(new PageType($pageRepo), $page);

        $form->handleRequest($Request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $page->saveForm();
            if ($page->getMenuOrder() == null) {
                $parentPageId = $page->getParentPage() !== null ? $page->getParentPage()->getId() : null;
                $maxMenuOrder = $em->getRepository('UczelniaPageBundle:Page')->getMaxMenuOrder($parentPageId);
                $page->setMenuOrder($maxMenuOrder !== null ? $maxMenuOrder + 10 : 0);
            }
            $em->persist($page);
            $pContentRepo = $em->getRepository('UczelniaPageBundle:PageContent');

            $whatChange = '';
            $contents = array();
            if (!empty($page->getContent()))
                foreach ($page->getCities() as $city) {
                    $pContent = $pContentRepo->getPageContent($page->getSlug(), $city->getSlug());
                    if ($pContent == null) {
                        $pContent = new PageContent();
                    }
                    $pContent->setContent($page->getContent())
                        ->setCity($city)
                        ->setPage($page)
                        ->setCreateDate(new \DateTime(null));
                    $contents[] = $pContent;
                    $whatChange .= $city->getName() . ', ';
                }

            foreach ($contents as $content) {
                $em->persist($content);
            }
            $em->flush();

            $logsRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:Log');
            $user = $this->get('security.context')->getToken()->getUser();
            if (!empty($whatChange)) {
                $whatChange = ' w '. substr($whatChange, 0, strlen($whatChange)-2);
            }
            if (isset($newPageForm)) {
                $logsRepo->addLog($user, 'Dodano nową stronę <i>'.$page->getTitle().'</i>'.$whatChange);
            } else {
                $logsRepo->addLog($user, 'Zmieniono stronę <i>' . $page->getTitle().'</i>'.$whatChange);
            }

            $message = (isset($newPageForm)) ? 'Poprawnie dodano nową stronę!' : 'Strona została zmieniona!';
            $this->get('session')->getFlashBag()->add('success', $message);

            return $this->redirect($this->generateUrl('admin_pageForm', array(
                'id' => $page->getId()
            )));
        }

        return array(
            'currPage' => 'pages',
            'form' => $form->createView(),
            'page' => $page
        );
    }

    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_pageDelete",
     *      requirements={"id"="\d+"}
     * )
     *
     * @Template()
     */
    public function deleteAction(Request $Request, $id)
    {
        $page = $this->getDoctrine()->getRepository('UczelniaPageBundle:Page')->find($id);
        if ($page == null) {
            throw new NotFoundException('Nie ma takiej strony!');
        }

        $form = $this->createFormBuilder()
            ->add('deleteWhat', 'choice', array(
                'choices' => array(
                    'wholePage' => 'Stronę ze wszystkich miast (wraz z zawartością)',
                    'partial' => 'Tylko zawartość z poszczególych miast'
                ),
                'data' => 'wholePage',
                'expanded' => true
            ))
            ->add('cities', 'entity', array(
                'multiple' => true,
                'expanded' => true,
                'class' => 'Uczelnia\PageBundle\Entity\City',
                'property' => 'name'
            ))
            ->add('pageId', 'hidden', array(
                'data' => $id
            ))
            ->add('delete', 'submit', array(
                'label' => 'Usuń'
            ))
            ->getForm();

        $form->handleRequest($Request);
        if ($form->isValid()) {
            $formData = $form->getData();
            $em = $this->getDoctrine()->getManager();

            if ($formData['deleteWhat'] == 'wholePage') {
                $em = $this->getDoctrine()->getManager();
                $em->remove($page);
                $em->flush();

                $logsRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:Log');
                $user = $this->get('security.context')->getToken()->getUser();
                $logsRepo->addLog($user, 'Usunięto stronę <i>'.$page->getTitle().'</i> z wszystkich miast!');

                $this->get('session')->getFlashBag()->add('success', 'Poprawnie usunięto stronę z wszystkich miast!');
            } else if ($formData['deleteWhat'] == 'partial') {
                $pContentRepo = $em->getRepository('UczelniaPageBundle:PageContent');
                $deletedFrom = '';

                foreach ($formData['cities'] as $city) {
                    $pContent = $pContentRepo->getPageContent($page->getSlug(), $city->getSlug());
                    if ($pContent != null) {
                        $em->remove($pContent);
                        $deletedFrom .= $city->getName() . ', ';
                    }
                }
                $em->flush();

                if (!empty($deletedFrom)) {
                    $deletedFrom = substr($deletedFrom, 0, strlen($deletedFrom)-2);
                }
                $logsRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:Log');
                $user = $this->get('security.context')->getToken()->getUser();
                $logsRepo->addLog($user, 'Usunięto stronę <i>'.$page->getTitle().'</i> z miast: '.$deletedFrom);

                $this->get('session')->getFlashBag()->add('success', 'Poprawnie usunięto stronę z miast: ' . $deletedFrom);
            }

            return $this->redirect($this->generateUrl('pages_archive'));
        }

        return array(
            'page' => $page->getTitle(),
            'form' => $form->createView()
        );
    }


    public static function getPagesHierarchicalList($pageRepo, $singlePrefix = '-') {
        $pages = $pageRepo->getPages();
        $pagesArray = array();

        foreach ($pages as $idx => $page) {
            PagesController::generatePagesArray($page, $pagesArray, '', $singlePrefix);
        }
        return $pagesArray;
    }

    private static function generatePagesArray($page, &$pagesArray, $prefix, $singlePrefix)
    {
        $page->setTitleWithPrefix(' ' . $prefix . ' ' . $page->getTitle());
        $pagesArray[] = $page;

        $subPages = $page->getPages();
        if ($subPages !== NULL) {
            foreach ($subPages as $subPage) {
                PagesController::generatePagesArray($subPage, $pagesArray, $prefix.$singlePrefix, $singlePrefix);
            }
        }
    }
}
