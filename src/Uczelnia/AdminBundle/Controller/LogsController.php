<?php

namespace Uczelnia\AdminBundle\Controller;

use FM\ElfinderBundle\Form\Type\ElFinderType;
use GuzzleHttp\Psr7\Response;
use Intervention\Image\Exception\NotFoundException;
use Uczelnia\AdminBundle\Form\Type\NewsType;
use Uczelnia\PageBundle\Entity\News;
use Uczelnia\PageBundle\Entity\Page;
use Uczelnia\PageBundle\Entity\PageContent;
use Uczelnia\PageBundle\Repository\PageContentRepository;
use Uczelnia\AdminBundle\Form\Type\PageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class LogsController extends Controller
{
    protected $itemsLimit = 10;

    /**
     * @Route("/historia/{page}",
     *     name="admin_history_archive",
     *      requirements={"page"="\d+"},
     *      defaults={"page"=1}
     * )
     */
    public function indexAction($page)
    {
        $logsRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:Log');
        $qb = $logsRepo->getQueryBuilder();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($qb, $page, $this->itemsLimit);

        $csrfProvider = $this->get('form.csrf_provider');

        return $this->render('UczelniaAdminBundle:Logs:index.html.twig', array(
            'pagination' => $pagination
        ));
    }

}
