<?php

namespace Uczelnia\AdminBundle\Controller;

use Common\UserBundle\Entity\User;
use Common\UserBundle\Entity\UserPassword;
use FM\ElfinderBundle\Form\Type\ElFinderType;
use GuzzleHttp\Psr7\Response;
use Intervention\Image\Exception\NotFoundException;
use Uczelnia\AdminBundle\Form\Type\ManageUserType;
use Uczelnia\AdminBundle\Form\Type\NewsType;
use Uczelnia\PageBundle\Entity\News;
use Uczelnia\PageBundle\Entity\Page;
use Uczelnia\PageBundle\Entity\PageContent;
use Uczelnia\PageBundle\Repository\PageContentRepository;
use Uczelnia\AdminBundle\Form\Type\PageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class UsersController extends Controller
{
    protected $itemsLimit = 10;

    /**
     * @Route("/uzytkownicy/{page}",
     *     name="admin_users_archive",
     *      requirements={"page"="\d+"},
     *      defaults={"page"=1}
     * )
     *
     * @Template
     */
    public function indexAction($page)
    {
        $userRepo = $this->getDoctrine()->getRepository('CommonUserBundle:User');
        $qb = $userRepo->getQueryBuilder();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($qb, $page, $this->itemsLimit);

        $csrfProvider = $this->get('form.csrf_provider');

        return array(
            'pagination' => $pagination,
            'csrfProvider' => $csrfProvider,
            'tokenName' => 'delUser%d'
        );
    }

    /**
     * @Route(
     *      "/uzytkownicy/form/{id}",
     *      name="admin_userForm",
     *      requirements={"id"="\d+"},
     *      defaults={"id"=NULL}
     * )
     *
     * @Template()
     */
    public function formAction(Request $Request, User $user = null)
    {
        if (null == $user) {
            $user = new User();
            $newUserForm = TRUE;
        }

        $pageRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:Page');
        $form = $this->createForm(new ManageUserType($pageRepo), $user);

        $form->handleRequest($Request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //$user->saveForm();

            if (!empty($user->getPlainPassword())) {
                $encoder = $this->container->get('security.password_encoder');
                $encoderPassword = $encoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($encoderPassword);
            } else if (isset($newUserForm)) {
                $this->get('session')->getFlashBag()->add('error', 'Musisz nadać hasło nowemu użytkownikowi');
                return array(
                    'currPage' => 'users',
                    'form' => $form->createView(),
                    'user' => $user
                );
            }
            $user->setEnabled(true);
            if (isset($newUserForm)) {
                $user->setRoles(array('ROLE_EDITOR'));
                $user->setUpdateDate(new \DateTime());
            } else {
                $userRepo = $this->getDoctrine()->getRepository('CommonUserBundle:User');
                $userInDb = $userRepo->findOneById($user->getId());
                $user->setRoles($userInDb->getRoles());
            }

            $em->persist($user);
            $em->flush();

            $message = (isset($newUserForm)) ? 'Poprawnie dodano nowego użytkownika!' : 'Użytkownik został zmieniony!';
            $this->get('session')->getFlashBag()->add('success', $message);

            return $this->redirect($this->generateUrl('admin_users_archive'));
        }

        return array(
            'currPage' => 'users',
            'form' => $form->createView(),
            'user' => $user
        );
    }

    /**
     * @Route(
     *      "/uzytkownicy/usun/{userId}/{token}",
     *      name="admin_userDelete",
     *      requirements={"id"="\d+"}
     * )
     */
    public function deleteAction($userId, $token)
    {
        if (!$this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException('Nie masz uprawnień do tego zadania!');
        }

        $validToken = sprintf('delUser%d', $userId);
        if (!$this->get('form.csrf_provider')->isCsrfTokenValid($validToken, $token)) {
            throw $this->createAccessDeniedException('Błędy token akcji.');
        }

        $user = $this->getDoctrine()->getRepository('CommonUserBundle:User')->find($userId);
        if ($user == null) {
            throw new NotFoundException('Nie ma takiej strony!');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return new \Symfony\Component\HttpFoundation\JsonResponse(array(
            'status' => 'ok',
            'message' => 'Użytkownik został usunięty'
        ));
    }
}
