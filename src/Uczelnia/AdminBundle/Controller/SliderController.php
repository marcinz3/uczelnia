<?php

namespace Uczelnia\AdminBundle\Controller;

use FM\ElfinderBundle\Form\Type\ElFinderType;
use GuzzleHttp\Psr7\Response;
use Intervention\Image\Exception\NotFoundException;
use Uczelnia\AdminBundle\Form\Type\NewsType;
use Uczelnia\AdminBundle\Form\Type\SlideType;
use Uczelnia\PageBundle\Entity\News;
use Uczelnia\PageBundle\Entity\Page;
use Uczelnia\PageBundle\Entity\PageContent;
use Uczelnia\PageBundle\Entity\Slide;
use Uczelnia\PageBundle\Repository\PageContentRepository;
use Uczelnia\AdminBundle\Form\Type\PageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class SliderController extends Controller
{
    /**
     * @Route("/slajder/", name="admin_slides_archive")
     *
     * @Template
     */
    public function indexAction()
    {
        $slidesRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:Slide');
        $slides = $slidesRepo->findAll();

        $csrfProvider = $this->get('form.csrf_provider');

        return array(
            'csrfProvider' => $csrfProvider,
            'tokenName' => 'delSlide%d',
            'slides' => $slides
        );
    }

    /**
     * @Route(
     *      "/slajder/form/{id}",
     *      name="admin_slideForm",
     *      requirements={"id"="\d+"},
     *      defaults={"id"=NULL}
     * )
     *
     * @Template()
     */
    public function formAction(Request $Request, Slide $slide = null)
    {
        if (null == $slide) {
            $slide = new Slide();
            $newSlideForm = TRUE;
        }

        $form = $this->createForm(new SlideType(), $slide);

        $form->handleRequest($Request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($slide->getSlideImg() == null) {
                $message = 'Muisz wybrać obraz!';
                $this->get('session')->getFlashBag()->add('error', $message);

                return array(
                    'currPage' => 'pages',
                    'form' => $form->createView(),
                    'slide' => $slide
                );
            }

            if ($slide->getSlideOrder() == null) {
                $maxSlideOrder = $em->getRepository('UczelniaPageBundle:Slide')->getMaxSlideOrder();
                $slide->setSlideOrder($maxSlideOrder !== null ? $maxSlideOrder + 10 : 0);
            }

            $em->persist($slide);
            $em->flush();

            $message = (isset($newSlideForm)) ? 'Poprawnie dodano nowy slajd!' : 'Slajd został zmieniony!';
            $this->get('session')->getFlashBag()->add('success', $message);

            return $this->redirect($this->generateUrl('admin_slides_archive'));
        }

        return array(
            'currPage' => 'pages',
            'form' => $form->createView(),
            'slide' => $slide
        );
    }

    /**
     * @Route(
     *      "/slajder/delete/{slideId}/{token}",
     *      name="admin_slideDelete",
     *      requirements={"id"="\d+"}
     * )
     */
    public function deleteAction($slideId, $token)
    {
        if (!$this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException('Nie masz uprawnień do tego zadania!');
        }

        $validToken = sprintf('delSlide%d', $slideId);
        if (!$this->get('form.csrf_provider')->isCsrfTokenValid($validToken, $token)) {
            throw $this->createAccessDeniedException('Błędy token akcji.');
        }

        $slide = $this->getDoctrine()->getRepository('UczelniaPageBundle:Slide')->find($slideId);
        if ($slide == null) {
            throw new NotFoundException('Nie ma takiej strony!');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($slide);
        $em->flush();

        return new \Symfony\Component\HttpFoundation\JsonResponse(array(
            'status' => 'ok',
            'message' => 'Slajd został usunięty'
        ));
    }

}
