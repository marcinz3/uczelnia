<?php

namespace Uczelnia\PageBundle\Twig\Extension;

use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Uczelnia\PageBundle\UczelniaPageBundle;
use Uczelnia\PageBundle\Libs\Utils;
use Symfony\Component\Security\Core\SecurityContext;


class PageExtension extends \Twig_Extension
{

    /**
     * @var Doctrine
     */
    private $doctrine;

    /**
     * @var SecurityContext
     */
    private $securityContext;

    /**
     *
     * @var \Twig_Environment
     */
    private $environment;

    private $citiesList;
    private $pagesList;


    function __construct(Doctrine $doctrine, SecurityContext $securityContext)
    {
        $this->doctrine = $doctrine;
        $this->securityContext = $securityContext;
    }

    public function initRuntime(\Twig_Environment $environment)
    {
        $this->environment = $environment;
    }

    public function getName()
    {
        return 'uczelnia_page_extension';
    }


    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('print_cities_list', array($this, 'printCitiesList'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('print_cities_colors_css', array($this, 'printCitiesColorStyles'), array('is_safe' => array('css'))),
            new \Twig_SimpleFunction('print_main_menu', array($this, 'printMainMenu'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('get_sub_pages', array($this, 'printMainMenu'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('print_right_menu', array($this, 'printRightMenu'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('print_right_news_archive', array($this, 'printRightNewsArchive'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('print_social_media_links', array($this, 'printSocialMediaLinks'), array('is_safe' => array('html')))
        );
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('ab_shorten', array($this, 'shorten'), array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('uczelnia_sluggify', array($this, 'sluggify'), array('is_safe' => array('html')))
        );
    }

    public function shorten($text, $length = 200, $wrapTag = 'p')
    {
        $text = strip_tags($text);
        if (strlen($text) > $length) {
            $text = substr($text, 0, $length) . '[...]';
        }
        $openTag = "<{$wrapTag}>";
        $closeTag = "</{$wrapTag}>";

        return $openTag . $text . $closeTag;
    }

    public function sluggify($text)
    {
        return Utils::sluggify($text);
    }

    public function getSubPages($pageSlug, $citySlug)
    {
        $pagesRepo = $this->doctrine->getRepository('UczelniaPageBundle:Page');
        $this->pagesList = $pagesRepo->getSubPages($pageSlug, $citySlug);

        return $this->environment->render('UczelniaPageBundle:Template:pagesList.html.twig', array(
            'pagesList' => $this->pagesList
        ));
    }

    public function printSocialMediaLinks()
    {
        if (!isset($this->citiesList)) {
            $citiesRepo = $this->doctrine->getRepository('UczelniaPageBundle:City');
            $this->citiesList = $citiesRepo->findAll();
        }

        return $this->environment->render('UczelniaPageBundle:Template:socialMediaLinks.html.twig', array(
            'citiesList' => $this->citiesList
        ));
    }

    public function printRightNewsArchive($citySlug)
    {
        $newsRepo = $this->doctrine->getRepository('UczelniaPageBundle:News');
        $newsArchive = $newsRepo->getArchive($citySlug);
        $result = array();

        $months_pl = array(
            'zero', 'Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec',
            'Lipiec', 'Sierpień', 'Wwrzesień', 'Październik', 'Listopad', 'Grudzień'
        );

        foreach ($newsArchive as $val) {
            $result[$val['year']][] = $months_pl[$val['month']];
        }

        return $this->environment->render('UczelniaPageBundle:Template:newsRightMenu.html.twig', array(
            'datesList' => $result
        ));
    }

    public function printMainMenu($citySlug)
    {
        $pagesRepo = $this->doctrine->getRepository('UczelniaPageBundle:Page');
        if (!isset($this->pagesList)) {
            $this->pagesList = $pagesRepo->getPagesInMenu($citySlug);
        }

        $subPagesList = array();
        foreach ($this->pagesList as $page) {
            $subPages = $pagesRepo->getSubPages($page->getId(), $citySlug);
            $subPagesList[$page->getSlug()] = $subPages !== null ? $subPages : array();
        }

        return $this->environment->render('UczelniaPageBundle:Template:pagesList.html.twig', array(
            'pagesList' => $this->pagesList,
            'subPagesList' => $subPagesList
        ));
    }

    public function printRightMenu($pageSlug, $citySlug)
    {
        $pagesRepo = $this->doctrine->getRepository('UczelniaPageBundle:Page');
        $page = $pagesRepo->getPage($pageSlug);
        $parentPages = array();
        $parentPagesCat = array();
        $subParentNeighbourPages = array();
        $subParentNeighbourHierarchy = '';
        $tmpPage = $page;
        $tmpPageCat = '';

        $subPages = $pagesRepo->getSubPages($page->getId(), $citySlug);
        $categories = '';
        if ($page->getParentPage() !== null) {
            if ($page->getParentPage()->getParentPage() !== null) {
                $subParentNeighbourPages = $pagesRepo->getSubPages($page->getParentPage()->getParentPage(), $citySlug);
                $subParentNeighbourPages = Utils::remove_array_item($subParentNeighbourPages, $page->getParentPage());
                $subParentNeighbourHierarchy = $page->getParentPage()->getParentPage()->getSlug();
            } else if (count($subPages) > 0) {
                $subParentNeighbourPages = $pagesRepo->getSubPages($page->getParentPage(), $citySlug);
                $subParentNeighbourPages = Utils::remove_array_item($subParentNeighbourPages, $page);
                $subParentNeighbourHierarchy = $page->getParentPage()->getSlug();
            }
        }
        if ($subPages == null && $page->getParentPage() != null) {
            $subPages = $pagesRepo->getSubPages($page->getParentPage() !== null ? $page->getParentPage()->getId() : null, $citySlug);

        } else {
//            $categories = '/' . $page->getSlug();
            $parentPages[] = $tmpPage;

        }

        while ($tmpPage->getParentPage() !== null) {
            $tmpPage = $tmpPage->getParentPage();
            $parentPages[] = $tmpPage;
        }

//        $parentPages[] = $page;

        rtrim($categories, '/');
        $categories = substr($categories, 1);
        //print_r($subPages);

        return $this->environment->render('UczelniaPageBundle:Template:pageRightMenu.html.twig', array(
            'parentPagesList' => array_reverse($parentPages),
            'pagesList' => $subPages,
            'subParentNeighbourHierarchy' => $subParentNeighbourHierarchy,
            'subParentNeighbourPages' => $subParentNeighbourPages
        ));
    }

    public function printCitiesList()
    {
        if (!isset($this->citiesList)) {
            $citiesRepo = $this->doctrine->getRepository('UczelniaPageBundle:City');
            $this->citiesList = $citiesRepo->findAll();
        }

        return $this->environment->render('UczelniaPageBundle:Template:citiesList.html.twig', array(
            'citiesList' => $this->citiesList
        ));
    }

    public function printCitiesColorStyles()
    {
        if (!isset($this->citiesList)) {
            $citiesRepo = $this->doctrine->getRepository('UczelniaPageBundle:City');
            $this->citiesList = $citiesRepo->findAll();
        }

        return $this->environment->render('UczelniaPageBundle:Template:citiesColorStyles.html.twig', array(
            'citiesList' => $this->citiesList
        ));
    }
}
