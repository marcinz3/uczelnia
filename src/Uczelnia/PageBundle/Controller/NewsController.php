<?php

namespace Uczelnia\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class NewsController extends Controller
{
    protected $itemsLimit = 5;

    /**
     * @Route(
     *     "/aktualnosci",
     *     name = "global_news_archive_first_page"
     * )
     */
    public function globalNewsArchiveFirstPageAction($page)
    {
        return $this->forward('UczelniaPageBundle:News:newsArchive', array(
            'city' => 'glowna',
            'page' => 1
        ));
    }

    /**
     * @Route(
     *     "/aktualnosci/{page}",
     *     name = "global_news_archive",
     *     defaults = {"page" = 1},
     *     requirements = {"page" = "\d+"}
     * )
     */
    public function globalNewsArchiveAction($page)
    {
        return $this->forward('UczelniaPageBundle:News:newsArchive', array(
            'city' => 'glowna',
            'page' => $page
        ));
    }

    /**
     * @Route(
     *     "/aktualnosci/{slug}",
     *     name = "global_news_single"
     * )
     */
    public function globalNewsSingleAction($slug)
    {
        return $this->forward('UczelniaPageBundle:News:newsSingle', array(
            'city' => 'glowna',
            'slug' => $slug
        ));
    }

    /**
     * @Route(
     *     "/aktualnosci/rok-{year}/{month}/{page}",
     *     name = "global_news_month_archive",
     *     defaults = {"page" = 1},
     *     requirements = {"year" = "\d\d\d\d", "month"="[a-z]+", "page" = "\d+"}
     * )
     */
    public function globalNewsInMonthArchiveAction($year, $month, $page) {
        return $this->forward('UczelniaPageBundle:News:newsInMonthArchive', array(
            'city' => 'glowna',
            'year' => $year,
            'month' => $month,
            'page' => $page
        ));
    }

    /**
     * @Route(
     *     "/{city}/aktualnosci/{page}",
     *     name = "news_archive",
     *     defaults = {"page" = 1},
     *     requirements = {"page" = "\d+"}
     * )
     *
     * @Template()
     */
    public function newsArchiveAction($city, $page)
    {
        $newsRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:News');
        $qb = $newsRepo->getQueryBuilder(array('citySlug' => $city));

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($qb, $page, $this->itemsLimit);

        $cityRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:City');
        $cityName = $cityRepo->getNameBySlug($city);

        return array(
            'city' => $cityName,
            'slug' => 'aktualnosci',
            'pagination' => $pagination
        );
    }

    /**
     * @Route(
     *     "/{city}/aktualnosci/rok-{year}/{month}/{page}",
     *     name = "news_month_archive",
     *     defaults = {"page" = 1},
     *     requirements = {"year" = "\d\d\d\d", "month"="[a-z]+", "page" = "\d+"}
     * )
     */
    public function newsInMonthArchiveAction($city, $year, $month, $page)
    {
        $months_pl = array(
            'styczen' => 1, 'luty' => 2, 'marzec' => 3, 'kwiecien' => 4,
            'maj' => 5, 'czerwiec' => 6, 'lipiec' => 7, 'sierpien' => 8,
            'wrzesien' => 9, 'pazdziernik' => 10, 'listopad' => 11, 'grudzien' => 12
        );

        if (!isset($months_pl[$month])) {
            throw $this->createNotFoundException('Strona nie została odnaleziona.');
        }

        $newsRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:News');
        $qb = $newsRepo->getQueryBuilder(array(
            'citySlug' => $city,
            'year' => $year,
            'month' => $months_pl[$month]
        ));

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($qb, $page, $this->itemsLimit);

        $cityRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:City');
        $cityName = $cityRepo->getNameBySlug($city);

        return $this->render('UczelniaPageBundle:News:newsArchive.html.twig', array(
            'city' => $cityName,
            'slug' => 'aktualnosci',
            'pagination' => $pagination
        ));
    }

    /**
     * @Route(
     *     "/{city}/aktualnosci/{slug}",
     *     name = "news_single",
     *     defaults = {"city" = "glowna"}
     * )
     *
     * @Template()
     */
    public function newsSingleAction($city, $slug)
    {
        $newsRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:News');
        $news = $newsRepo->getOneNews($slug);

        if (null === $news) {
            throw $this->createNotFoundException('Strona nie została odnaleziona.');
        }

        $cityRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:City');
        $cityName = $cityRepo->getNameBySlug($city);

        return array(
            'city' => $cityName,
            'categories' => 'aktualnosci',
            'news' => $news
        );
    }
}
