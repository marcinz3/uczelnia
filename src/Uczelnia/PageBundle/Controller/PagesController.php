<?php

namespace Uczelnia\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Null;

class PagesController extends Controller
{
    /**
     * @Route("/", name="page_index")
     *
     * @Template()
     */
    public function indexAction()
    {
        $newsRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:News');
        $news = $newsRepo->getQueryBuilder(array('citySlug' => 'glowna'))->setMaxResults(3)->getQuery()->getResult();

        $slidesRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:Slide');
        $slides = $slidesRepo->findAll();

        return array(
            'slidesList' => $slides,
            'newsList' => $news
        );
    }

    /**
     * @Route("/{city}", name="city_news_or_single_page")
     */
    public function cityNewsOrSinglePageAction($city)
    {
        // Redirect to city/news
        // or if not a city, render a single page
        $cityRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:City');
        if ($cityRepo->findOneBySlug($city) !== null) {
            return $this->redirectToRoute('news_archive', array('city' => $city));
        } else {
            return $this->forward('UczelniaPageBundle:Pages:page', array(
                'city' => 'glowna',
                'categories' => NULL,
                'slug' => $city
            ));
        }
    }

    /**
     * @Route("/{city}/{slug}", name="first_lvl_city_single")
     */
    public function cityPageAction($city, $slug)
    {
        return $this->forward('UczelniaPageBundle:Pages:page', array(
            'city' => $city,
            'categories' => NULL,
            'slug' => $slug
        ));
        //return $this->renderPage($city, $slug);
    }

    /**
     * @Route("/{city}/{categories}/{slug}", requirements={"categories" = ".+"}, name="subpage_city_single")
     *
     * @Template()
     */
    public function pageAction($city, $categories, $slug)
    {
        if ($categories !== NULL) {
            $categories = explode('/', $categories);
            $pageRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:Page');
            if ($pageRepo->isExistPage($slug, $categories) === NULL) {
                throw $this->createNotFoundException('Strona nie została odnaleziona.');
            }
        }

        return $this->renderPage($city, $slug);
    }

    private function renderPage($city, $slug)
    {
        $cityRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:City');
        $cityName = $cityRepo->getNameBySlug($city);

        $pageContentRepo = $this->getDoctrine()->getRepository('UczelniaPageBundle:PageContent');
        $pageContent = $pageContentRepo->getPageContent($slug, $city);

        $user = $this->getUser();
        if ($this->get('security.context')->isGranted('ROLE_ADMIN') || ($this->get('security.context')->isGranted('ROLE_EDITOR') && $user->getCities()->contains($pageContent->getCity()) && $user->getPages()->contains($pageContent->getPage()))) {
            return $this->forward('UczelniaEditBundle:Editor:editorPage', array(
                'city' => $city,
                'slug' => $slug
            ));
        }

        if (null === $pageContent) {
            throw $this->createNotFoundException('Strona nie została odnaleziona.');
        }

        return $this->render('@UczelniaPage/Pages/page.html.twig', array(
            'city' => $cityName,
            'title' => $pageContent->getPage()->getTitle(),
            'mainContent' => $pageContent->getContent()
        ));
    }
}
