<?php

namespace Uczelnia\PageBundle\Repository;

use Doctrine\ORM\EntityRepository;
use \Doctrine\ORM\Internal\Hydration\ObjectHydrator;
use \Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\Validator\Constraints\DateTime;


class NewsRepository extends EntityRepository
{
    public function findAll()
    {
        return $this->findBy(array(), array('createDate' => 'DESC'));
    }

    public function getOneNews($slug)
    {
        $qb = $this->createQueryBuilder('n');

        $qb->where('n.slug = :slug')
            ->setParameter('slug', $slug);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getQueryBuilder(array $params = array())
    {
        $qb = $this->createQueryBuilder('n')
            ->select('n')
            ->leftJoin('n.cities', 'c')
            ->orderBy('n.createDate', 'DESC');
        if (!empty($params['citySlug'])) {
            $qb->andWhere('c.slug = :citySlug')
                ->setParameter('citySlug', $params['citySlug']);
        }

        if (!empty($params['year']) && !empty($params['month'])) {
            $query_date = $params['year'] . '-' . $params['month'] . '-01';
            $fromDate = date('Y-m-01', strtotime($query_date));
            $toDate = date('Y-m-t', strtotime($query_date));

            $qb->andWhere('n.createDate BETWEEN :from AND :to')
                ->setParameter('from', $fromDate)
                ->setParameter('to', $toDate);
        }

        return $qb;
    }

    public function getArchive($citySlug)
    {
//        $qb = $this->createQueryBuilder('n')
//            ->select('YEAR(n.createDate)')
//            ->leftJoin('n.cities', 'c')
//            ->where('c.slug = :citySlug')
//                ->setParameter('citySlug', $citySlug)
        ;//->orderBy('year', 'DESC');
        //->setMaxResults($limit);

//        return $qb->getQuery()->getScalarResult();
        $sql = 'SELECT DISTINCT YEAR(create_date) AS year, MONTH(create_date) AS month 
                  FROM uczelnia_news AS n INNER JOIN news_city AS nc ON nc.news_id = n.id 
                        INNER JOIN uczelnia_cities AS c ON nc.city_id = c.id 
                  WHERE c.slug = :citySlug ORDER BY year DESC, month ASC';
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue('citySlug', $citySlug);
        $stmt->execute();
        return $stmt->fetchAll();
        //->createNativeQuery('SELECT YEAR(n.createDate) FROM uczelnia_news')->getArrayResult();
    }
}
