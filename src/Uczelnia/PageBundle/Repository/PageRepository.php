<?php

namespace Uczelnia\PageBundle\Repository;

use Doctrine\ORM\EntityRepository;
use \Doctrine\ORM\Internal\Hydration\ObjectHydrator;
use \Doctrine\ORM\PersistentCollection;


class PageRepository extends EntityRepository
{
    public function getPagesInMenu($citySlug)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->innerJoin('p.pageContents', 'pc')
            ->innerJoin('pc.city', 'city');
        $qb->where($qb->expr()->gt('p.menuOrder', -1))
            ->andWhere($qb->expr()->isNull('p.parentPage'))
            ->andWhere('city.slug = :citySlug')
            ->setParameter('citySlug', $citySlug)
            ->orderBy('p.menuOrder', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function getSubPages($pageId, $citySlug)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->innerJoin('p.pageContents', 'pc')
            ->innerJoin('pc.city', 'city');
        $qb->where($qb->expr()->gt('p.menuOrder', -1));
        if ($pageId !== null) {
            $qb->andWhere('p.parentPage = :parentPageId')
                ->setParameter('parentPageId', $pageId);
        } else {
            $qb->andWhere($qb->expr()->isNull('p.parentPage'));
        }
        $qb->andWhere('city.slug = :citySlug')
            ->setParameter('citySlug', $citySlug)
            ->orderBy('p.menuOrder', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function getPages()
    {
        $qb = $this->createQueryBuilder('p')
            ->select('p');
        $qb->where($qb->expr()->isNull('p.parentPage'))
            ->orderBy('p.menuOrder', 'ASC');
        $res = $qb->getQuery()->getResult();

        return ($res);
    }

    public function getMaxMenuOrder($parentPageId)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('MAX(p.menuOrder) AS maxMenuOrder');
        if ($parentPageId !== null) {
            $qb->where('p.parentPage = :parentPageId')
                ->setParameter('parentPageId', $parentPageId);
        } else {
            $qb->where($qb->expr()->isNull('p.parentPage'));
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getPage($slug)
    {
        $qb = $this->getQueryBuilder();

        $qb->andWhere('p.slug = :slug')
            ->setParameter('slug', $slug);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function isExistPage($slug, $parents)
    {
        $qb = $this->getQueryBuilder();

        $lastIdx = '';
        foreach (array_reverse($parents) as $idx => $parent) {
            $qb->innerJoin('p' . $lastIdx . '.parentPage', 'p' . $idx);
            $qb->andWhere('p' . $idx . '.slug = :slug' . $idx)
                ->setParameter('slug' . $idx, $parent);
            $lastIdx = $idx;
        }

        $qb->andWhere('p.slug = :slug')
            ->setParameter('slug', $slug);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getQueryBuilder(array $params = array())
    {

        $qb = $this->createQueryBuilder('p')
            ->select('p, parent')
            ->leftJoin('p.parentPage', 'parent');

        if (!empty($params['orderBy'])) {
            $orderDir = !empty($params['orderDir']) ? $params['orderDir'] : NULL;
            $qb->orderBy($params['orderBy'], $orderDir);
        }

        if (!empty($params['parentSlug'])) {
            $qb->andWhere('parent.slug = :parentSlug')
                ->setParameter('parentSlug', $params['parentSlug']);
        }

        return $qb;
    }
}
