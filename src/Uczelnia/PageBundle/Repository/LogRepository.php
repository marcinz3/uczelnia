<?php

namespace Uczelnia\PageBundle\Repository;

use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Uczelnia\PageBundle\Entity\Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * LogRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LogRepository extends \Doctrine\ORM\EntityRepository
{
    public function findAll()
    {
        return $this->findBy(array(), array('eventDate' => 'DESC'));
    }

    public function addLog($user, $message)
    {
        $em = $this->getEntityManager();
        $newLog = new Log();
        $newLog->setAuthor($user);
        $newLog->setMessage($message);

        $em->persist($newLog);
        $em->flush();
    }

    public function getQueryBuilder(array $params = array())
    {
        $qb = $this->createQueryBuilder('l')
            ->orderBy('l.eventDate', 'DESC');

        return $qb;
    }
}
