<?php

namespace Uczelnia\PageBundle\Repository;

use Doctrine\ORM\EntityRepository;


class PageContentRepository extends EntityRepository
{

    public function getPageContent($slug, $city)
    {
        $qb = $this->getQueryBuilder(array('pageSlug' => $slug, 'citySlug' => $city));

        return $qb->getQuery()->getOneOrNullResult();
    }


    public function getQueryBuilder(array $params = array())
    {
        $qb = $this->createQueryBuilder('p')
            ->select('p, page, c')
            ->leftJoin('p.page', 'page')
            ->leftJoin('p.city', 'c');

        if (!empty($params['orderBy'])) {
            $orderDir = !empty($params['orderDir']) ? $params['orderDir'] : NULL;
            $qb->orderBy($params['orderBy'], $orderDir);
        }

        if (!empty($params['pageSlug'])) {
            $qb->andWhere('page.slug = :pageSlug')
                ->setParameter('pageSlug', $params['pageSlug']);
        }

        if (!empty($params['citySlug'])) {
            $qb->andWhere('c.slug = :citySlug')
                ->setParameter('citySlug', $params['citySlug']);
        }

        return $qb;
    }

}
