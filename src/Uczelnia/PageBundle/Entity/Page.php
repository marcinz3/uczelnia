<?php

namespace Uczelnia\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="Uczelnia\PageBundle\Repository\PageRepository")
 * @ORM\Table(name="uczelnia_pages")
 * @ORM\HasLifecycleCallbacks
 *
 * @UniqueEntity(fields={"title"})
 * @UniqueEntity(fields={"slug"})
 */
class Page
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=120, unique=true)
     *
     * @Assert\NotBlank
     *
     * @Assert\Length(
     *      max = 120
     * )
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=120, unique=true)
     *
     * @Assert\Length(
     *      max = 120
     * )
     */
    private $slug;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Page",
     *      inversedBy = "pages"
     * )
     *
     * @ORM\JoinColumn(
     *      name = "parent_page_id",
     *      referencedColumnName = "id",
     *      onDelete = "CASCADE"
     * )
     */
    private $parentPage;

    /**
     * @ORM\Column(name="create_date", type="datetime")
     */
    private $createDate;

    /**
     * @ORM\Column(name="update_date", type="datetime", nullable=true)
     */
    private $updateDate = null;

    /**
     * @ORM\Column(name="menu_order", type="integer", nullable=false, options={"default" : -1})
     */
    private $menuOrder;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "\Uczelnia\PageBundle\Entity\PageContent",
     *      mappedBy = "page"
     * )
     */
    private $pageContents;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Page",
     *      mappedBy = "parentPage"
     * )
     */
    private $pages;

    private $content;

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    private $cities;

    public function getCities()
    {
        return $this->cities;
    }

    public function setCities($cities)
    {
        $this->cities = $cities;
    }


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function preSave()
    {
        if (null === $this->slug) {
            $this->setSlug($this->getTitle());
        }

        if (null == $this->createDate) {
            $this->createDate = new \DateTime();
        }
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pageContents = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Page
     */
    public function setSlug($slug)
    {
        $this->slug = \Uczelnia\PageBundle\Libs\Utils::sluggify($slug);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return Page
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     *
     * @return Page
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set parentPageId
     *
     * @param \Uczelnia\PageBundle\Entity\Page $parentPageId
     *
     * @return Page
     */
    public function setParentPage(\Uczelnia\PageBundle\Entity\Page $parentPage = null)
    {
        $this->parentPage = $parentPage;

        return $this;
    }

    /**
     * Get parentPageId
     *
     * @return \Uczelnia\PageBundle\Entity\Page
     */
    public function getParentPage()
    {
        return $this->parentPage;
    }

    /**
     * Add pageContent
     *
     * @param \Uczelnia\PageBundle\Entity\PageContent $pageContent
     *
     * @return Page
     */
    public function addPageContent(\Uczelnia\PageBundle\Entity\PageContent $pageContent)
    {
        $this->pageContents[] = $pageContent;

        return $this;
    }

    /**
     * Remove pageContent
     *
     * @param \Uczelnia\PageBundle\Entity\PageContent $pageContent
     */
    public function removePageContent(\Uczelnia\PageBundle\Entity\PageContent $pageContent)
    {
        $this->pageContents->removeElement($pageContent);
    }

    /**
     * Get pageContents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPageContents()
    {
        return $this->pageContents;
    }

    /**
     * Set menuOrder
     *
     * @param integer $menuOrder
     *
     * @return Page
     */
    public function setMenuOrder($menuOrder)
    {
        $this->menuOrder = $menuOrder;

        return $this;
    }

    /**
     * Get menuOrder
     *
     * @return integer
     */
    public function getMenuOrder()
    {
        return $this->menuOrder;
    }

    /**
     * Add page
     *
     * @param \Uczelnia\PageBundle\Entity\Page $page
     *
     * @return Page
     */
    public function addPage(\Uczelnia\PageBundle\Entity\Page $page)
    {
        $this->pages[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Uczelnia\PageBundle\Entity\Page $page
     */
    public function removePage(\Uczelnia\PageBundle\Entity\Page $page)
    {
        $this->pages->removeElement($page);
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPages()
    {
        return $this->pages;
    }

    private $updateSlug = true;

    public function isUpdateSlug()
    {
        return $this->updateSlug;
    }

    public function setUpdateSlug($updateSlug)
    {
        $this->updateSlug = $updateSlug;
    }

    private $titleWithPrefix;

    /**
     * @return mixed
     */
    public function getTitleWithPrefix()
    {
        return $this->titleWithPrefix;
    }

    /**
     * @param mixed $titleWithPrefix
     */
    public function setTitleWithPrefix($titleWithPrefix)
    {
        $this->titleWithPrefix = $titleWithPrefix;
    }

    public function saveForm() {
        if ($this->isUpdateSlug()) {
            $this->setSlug($this->title);
        }
    }
}
