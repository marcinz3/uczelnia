<?php

namespace Uczelnia\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="Uczelnia\PageBundle\Repository\SlideRepository")
 * @ORM\Table(name="uczelnia_slider")
 * @ORM\HasLifecycleCallbacks
 *
 * @UniqueEntity(fields={"slideImg"})
 */
class Slide
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="create_date", type="datetime")
     */
    private $createDate;

    /**
     * @ORM\Column(name="update_date", type="datetime", nullable=true)
     */
    private $updateDate = null;

    /**
     * @ORM\Column(type="string", length=80, nullable=false)
     *
     */
    private $slideImg;

    /**
     * @ORM\Column(name="slide_order", type="integer", nullable=false, options={"default" : 0})
     */
    private $slideOrder;

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function preSave()
    {
        if (null == $this->createDate) {
            $this->createDate = new \DateTime();
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return News
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     *
     * @return News
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }


    /**
     * Set slideImg
     *
     * @param string $slideImg
     *
     * @return Slide
     */
    public function setSlideImg($slideImg)
    {
        $this->slideImg = $slideImg;

        return $this;
    }

    /**
     * Get slideImg
     *
     * @return string
     */
    public function getSlideImg()
    {
        return $this->slideImg;
    }

    /**
     * Set slideOrder
     *
     * @param integer $slideOrder
     *
     * @return Slide
     */
    public function setSlideOrder($slideOrder)
    {
        $this->slideOrder = $slideOrder;

        return $this;
    }

    /**
     * Get slideOrder
     *
     * @return integer
     */
    public function getSlideOrder()
    {
        return $this->slideOrder;
    }
}
