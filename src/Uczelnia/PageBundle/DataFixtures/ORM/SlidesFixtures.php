<?php

namespace Uczelnia\PageBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Uczelnia\PageBundle\Entity\City;
use Uczelnia\PageBundle\Entity\News;
use Uczelnia\PageBundle\Entity\Page;
use Uczelnia\PageBundle\Entity\PageContent;
use Uczelnia\PageBundle\Entity\Slide;
use Uczelnia\PageBundle\Libs\Utils;


class SlidesFixtures extends AbstractFixture implements OrderedFixtureInterface {

    public function getOrder() {
        return 0;
    }

    public function load(ObjectManager $manager) {

        $slidesList = array(
            array(
                'slideImg' => '/uploads/slide1.png',
                'slideOrder' => 10,
                'createDate' => '2012-01-01 12:12:12',
                'updateDate' => NULL
            ),
            array(
                'slideImg' => '/uploads/slide2.png',
                'slideOrder' => 20,
                'createDate' => '2012-01-01 12:12:12',
                'updateDate' => NULL
            ),
        );

        foreach ($slidesList as $idx => $details) {
            $slide = new Slide();

            $slide->setSlideImg($details['slideImg'])
                ->setSlideOrder($details['slideOrder'])
                ->setCreateDate(new \DateTime($details['createDate']))
                ->setUpdateDate(new \DateTime($details['updateDate']));

            $manager->persist($slide);
        }

        $manager->flush();
    }

}
