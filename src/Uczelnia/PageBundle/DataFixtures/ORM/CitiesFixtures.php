<?php

namespace Uczelnia\PageBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Uczelnia\PageBundle\Entity\City;
use Uczelnia\PageBundle\Entity\Page;
use Uczelnia\PageBundle\Libs\Utils;


class CitiesFixtures extends AbstractFixture implements OrderedFixtureInterface {

    public function getOrder() {
        return 0;
    }

    public function load(ObjectManager $manager) {

        $citiesList = array(
            array(
                'name' => 'Główna',
                'color' => '008bd2',
            ),
            array(
                'name' => 'Warszawa',
                'color' => '7368ab',
            ),
            array(
                'name' => 'Chełm',
                'color' => 'ed5f67',
            ),
            array(
                'name' => 'Elbląg',
                'color' => 'f99157',
            ),
            array(
                'name' => 'Kraków',
                'color' => 'fac863',
            ),
            array(
                'name' => 'Łomża',
                'color' => '99c794',
            ),
            array(
                'name' => 'Opole',
                'color' => '5eb4b3',
            ),
            array(
                'name' => 'Szczecin',
                'color' => 'c4c4c4',
            ),
            array(
                'name' => 'Zabrze',
                'color' => 'ab7968',
            ),
        );

        foreach ($citiesList as $idx => $details) {
            $city = new City();

            $city->setName($details['name'])
                ->setColor($details['color']);

            $this->addReference('city-'.Utils::sluggify($details['name']), $city);

            $manager->persist($city);
        }

        $manager->flush();
    }

}
