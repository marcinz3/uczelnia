<?php

namespace Uczelnia\PageBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Uczelnia\PageBundle\Entity\City;
use Uczelnia\PageBundle\Entity\News;
use Uczelnia\PageBundle\Entity\Page;
use Uczelnia\PageBundle\Entity\PageContent;
use Uczelnia\PageBundle\Libs\Utils;


class NewsFixtures extends AbstractFixture implements OrderedFixtureInterface {

    public function getOrder() {
        return 2;
    }

    public function load(ObjectManager $manager) {

        $newsList = array(
            array(
                'title' => 'Nowa wiadomość 1',
                'content' => 'fewf frgrg grwgr gwrr',
                'createDate' => '2012-01-01 12:12:12',
                'updateDate' => NULL,
                'cities' => array('city-warszawa', 'city-chelm'),
            ),
            array(
                'title' => 'Nowa wiadomość 2',
                'content' => 'daf dgsg rerew iuioui hjkh dsa',
                'createDate' => '2012-01-01 12:12:12',
                'updateDate' => NULL,
                'cities' => array('city-glowna'),
            ),
        );

        foreach ($newsList as $idx => $details) {
            $news = new News();

            $news->setContent($details['content'])
                ->setCreateDate(new \DateTime($details['createDate']))
                ->setUpdateDate(new \DateTime($details['updateDate']))
                ->setTitle($details['title']);
            foreach ($details['cities'] as $city)
                $news->addCity($this->getReference($city));

            //$this->addReference('pageContent-'.Utils::sluggify($details['title']), $pageContent);

            $manager->persist($news);
        }

        $manager->flush();
    }

}
