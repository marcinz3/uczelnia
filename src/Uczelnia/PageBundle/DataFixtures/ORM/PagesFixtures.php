<?php

namespace Uczelnia\PageBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Uczelnia\PageBundle\Entity\City;
use Uczelnia\PageBundle\Entity\Page;
use Uczelnia\PageBundle\Libs\Utils;


class PagesFixtures extends AbstractFixture implements OrderedFixtureInterface {

    public function getOrder() {
        return 1;
    }

    public function load(ObjectManager $manager) {

        $pagesList = array(
            array(
                'title' => 'Kontakt',
                'createDate' => '2012-01-01 12:12:12',
                'updateDate' => NULL,
                'parentPageId' => NULL,
                'menuOrder' => 6,
            ),
            array(
                'title' => 'Oferta edukacyjna',
                'createDate' => '2012-01-01 12:12:12',
                'updateDate' => NULL,
                'parentPageId' => NULL,
                'menuOrder' => 2,
            ),
            array(
                'title' => 'Projekty',
                'createDate' => '2012-01-01 12:12:12',
                'updateDate' => NULL,
                'parentPageId' => NULL,
                'menuOrder' => 3,
            ),
            array(
                'title' => 'O uczelni',
                'createDate' => '2012-01-01 12:12:12',
                'updateDate' => NULL,
                'parentPageId' => NULL,
                'menuOrder' => 5,
            ),
            array(
                'title' => 'Rekrutacja',
                'createDate' => '2012-01-01 12:12:12',
                'updateDate' => NULL,
                'parentPageId' => NULL,
                'menuOrder' => 1,
            ),
            array(
                'title' => 'Zasady rekrutacji',
                'createDate' => '2012-01-01 12:12:12',
                'updateDate' => NULL,
                'parentPageId' => 'page-rekrutacja',
                'menuOrder' => 1,
            ),
            array(
                'title' => 'Rekrutacja Online',
                'createDate' => '2012-01-01 12:12:12',
                'updateDate' => NULL,
                'parentPageId' => 'page-rekrutacja',
                'menuOrder' => 2,
            ),
            array(
                'title' => 'info',
                'createDate' => '2012-01-01 12:12:12',
                'updateDate' => '2016-01-01 14:33:09',
                'parentPageId' => NULL,
                'menuOrder' => -1,
            ),
        );

        foreach ($pagesList as $idx => $details) {
            $page = new Page();

            $page->setTitle($details['title'])
                ->setCreateDate(new \DateTime($details['createDate']))
                ->setUpdateDate(new \DateTime($details['updateDate']))
                ->setMenuOrder($details['menuOrder']);

            if ($details['parentPageId'] == NULL) {
                $page->setParentPage(NULL);
            } else {
                $page->setParentPage($this->getReference($details['parentPageId']));
            }

            $this->addReference('page-'.Utils::sluggify($details['title']), $page);

            $manager->persist($page);
        }

        $manager->flush();
    }

}
