<?php

namespace Uczelnia\PageBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Uczelnia\PageBundle\Entity\City;
use Uczelnia\PageBundle\Entity\Page;
use Uczelnia\PageBundle\Entity\PageContent;
use Uczelnia\PageBundle\Libs\Utils;


class PagContentsFixtures extends AbstractFixture implements OrderedFixtureInterface {

    public function getOrder() {
        return 2;
    }

    public function load(ObjectManager $manager) {

        $pagesList = array(
            array(
                'content' => 'Ingormacje kontaktowe dla Warszawy...',
                'createDate' => '2012-01-01 12:12:12',
                'updateDate' => NULL,
                'page' => 'page-kontakt',
                'city' => 'city-warszawa',
            ),
            array(
                'content' => 'Ingormacje kontaktowe dla Łomży...',
                'createDate' => '2012-01-01 12:12:12',
                'updateDate' => NULL,
                'page' => 'page-kontakt',
                'city' => 'city-lomza',
            ),
            array(
                'content' => 'Zasady <b>rekrutacji</b> dla Łomży...',
                'createDate' => '2012-01-01 12:12:12',
                'updateDate' => NULL,
                'page' => 'page-zasady-rekrutacji',
                'city' => 'city-lomza',
            ),
        );

        foreach ($pagesList as $idx => $details) {
            $pageContent = new PageContent();

            $pageContent->setContent($details['content'])
                ->setCreateDate(new \DateTime($details['createDate']))
                ->setUpdateDate(new \DateTime($details['updateDate']))
                ->setPage($this->getReference($details['page']))
                ->setCity($this->getReference($details['city']));

            //$this->addReference('pageContent-'.Utils::sluggify($details['title']), $pageContent);

            $manager->persist($pageContent);
        }

        $manager->flush();
    }

}
