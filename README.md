# Uczelnia #

Przykładowa aplikacja webowa napisana z myślą o uczelni posiadającej oddziały w kilku miastach napisana w PHP i Symfony2 Framework.

### Funkcjonalność ###

* Kokpit umożliwiający dodawanie różnej treści podstron dla różnych miast![Screen Shot 2017-06-29 at 00.24.33.png](https://bitbucket.org/repo/5qdb8ng/images/981174100-Screen%20Shot%202017-06-29%20at%2000.24.33.png)![Screen Shot 2017-06-29 at 10.40.30.png](https://bitbucket.org/repo/5qdb8ng/images/508660286-Screen%20Shot%202017-06-29%20at%2010.40.30.png)![Screen Shot 2017-06-29 at 10.36.49 kopia.jpg](https://bitbucket.org/repo/5qdb8ng/images/4216975169-Screen%20Shot%202017-06-29%20at%2010.36.49%20kopia.jpg)
* Możliwość nadawania uprawnień użytkownikom do edycji jedynie konkretnych podstron![Screen Shot 2017-06-29 at 00.24.51.png](https://bitbucket.org/repo/5qdb8ng/images/3992611435-Screen%20Shot%202017-06-29%20at%2000.24.51.png) ![Screen Shot 2017-06-29 at 10.35.02.png](https://bitbucket.org/repo/5qdb8ng/images/2102213858-Screen%20Shot%202017-06-29%20at%2010.35.02.png)
* Ważność haseł użytkowników wygasają, wtedy użytkownik zmuszony jest na zmiane hasła no nowe - inne niż poprzednie (pamiętana jest historia haseł użytkowników)![Screen Shot 2017-06-29 at 10.56.23.png](https://bitbucket.org/repo/5qdb8ng/images/3187347580-Screen%20Shot%202017-06-29%20at%2010.56.23.png)
* Każda zmiana powoduje zapis odpowiedniej informacji w historii - logach.![Screen Shot 2017-06-29 at 00.25.11.png](https://bitbucket.org/repo/5qdb8ng/images/3782590708-Screen%20Shot%202017-06-29%20at%2000.25.11.png)

### Jak uruchomić? ###

```
git clone https://marcinz3@bitbucket.org/marcinz3/uczelnia.git
```
* Instalacja composer'a
* Dodanie w pliku *app/config/parameters.yml*:
```
    use_assetic_controller: true
    locale: pl
```
* Następnie w terminalu
```
php app/console server:start
php app/console assets:install --symlink
php app/console cache:clear
```
* Można użyć: (ale lepiej skorzystać ze skryptu *baza-danych-skrypt.sql*)
```
php app/console doctrine:database:create
php app/console doctrine:schema:create
php app/console doctrine:fixtures:load
```
* Aby zalogować się do kokpitu należy wejść pod adres */logowanie*
```
Administrator:
login: mar3
hasło: TrudneHaslo321?

Użytkownik z możliwością edycji jedynie podstrony "Kontakt" oraz "O uczelni" w Warszawie i Chełmie:
login: nowy1
hasło: z12345678
```


### Autor ###

* Marcin Zając
* Wszelkie prawa zastrzeżone