CKEDITOR.editorConfig = function( config ) {
    config.toolbarGroups = [
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'clipboard', groups: [ 'undo', 'clipboard' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        { name: 'forms', groups: [ 'forms' ] },
        { name: 'tools', groups: [ 'tools' ] },
        { name: 'about', groups: [ 'about' ] },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'styles', groups: [ 'styles' ] },
        { name: 'colors', groups: [ 'colors' ] },
        { name: 'others', groups: [ 'others' ] },
        '/',
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'insert', groups: [ 'insert' ] }
    ];

    config.disableNativeSpellChecker = false;
    config.extraPlugins = 'inlinesave';
    config.inlinesave = {
        postUrl: '',
        postData: {test: true},
        onSave: function(editor) { return saveEditorForm(); },
        onSuccess: function(editor, data) { console.log('save successful', editor, data); },
        onFailure: function(editor, status, request) { console.log('save failed', editor, status, request); },
        useJSON: false,
        useColorIcon: false
    };

    config.contentsCss = [CKEDITOR.getUrl( 'contents.css' ), 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'];

    config.removeButtons = 'Templates,Save,Scayt,contextmenu,Form,Button,ImageButton,Checkbox,Radio,HiddenField,TextField,Textarea,Select,CreateDiv,Language,Flash,PageBreak,Iframe,Preview,SelectAll,CopyFormatting,RemoveFormat';
};
//
// CKEDITOR.plugins.addExternal( 'bootstrapTabs', '/plugins/bootstrapTabs/', 'plugin.js' );
//
// CKEDITOR.replace( 'page_content', {
//     extraPlugins: 'bootstrapTabs',
//     contentsCss: [ '' ],
//     on: {
//         instanceReady: loadBootstrap,
//         mode: loadBootstrap
//     }
// });
//
// // Add the necessary jQuery and Bootstrap JS source so that tabs are clickable.
// // If you're already using Bootstrap JS with your editor instances, then this is not necessary.
// function loadBootstrap(event) {
//     if (event.name == 'mode' && event.editor.mode == 'source')
//         return; // Skip loading jQuery and Bootstrap when switching to source mode.
//
//     var jQueryScriptTag = document.createElement('script');
//     var bootstrapScriptTag = document.createElement('script');
//
//     jQueryScriptTag.src = 'https://code.jquery.com/jquery-1.11.3.min.js';
//     bootstrapScriptTag.src = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js';
//
//     var editorHead = event.editor.document.$.head;
//
//     editorHead.appendChild(jQueryScriptTag);
//     jQueryScriptTag.onload = function() {
//         editorHead.appendChild(bootstrapScriptTag);
//     };
// }
