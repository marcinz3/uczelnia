-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2017 at 12:14 PM
-- Server version: 5.7.15
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uczelnia`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `get_lineage` (`the_id` INT) RETURNS TEXT CHARSET utf8 READS SQL DATA
BEGIN

 DECLARE v_rec INT DEFAULT 0;

 DECLARE done INT DEFAULT FALSE;
 DECLARE v_res text DEFAULT '';
 DECLARE v_papa int;
 DECLARE v_papa_papa int DEFAULT -1;
 DECLARE csr CURSOR FOR 
  select _id,parent_id   from 
    (SELECT @r AS _id,
        (SELECT @r := parent_page_id FROM uczelnia_pages WHERE id = _id) AS parent_id,
        @l := @l + 1 AS lvl
    FROM
        (SELECT @r := the_id, @l := 0,@n:=0) vars,
        uczelnia_pages m
    WHERE @r <> 0
    ) T1
    where T1.parent_id is not null
 ORDER BY T1.lvl DESC;
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    open csr;
    read_loop: LOOP
    fetch csr into v_papa,v_papa_papa;
        SET v_rec = v_rec+1;
        IF done THEN
            LEAVE read_loop;
        END IF;
                IF v_rec = 1 THEN
            SET v_res = v_papa_papa;
        END IF;
        SET v_res = CONCAT(v_res,'-',v_papa);
    END LOOP;
    close csr;
    return v_res;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `news_city`
--

CREATE TABLE `news_city` (
  `news_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news_city`
--

INSERT INTO `news_city` (`news_id`, `city_id`) VALUES
(9, 98),
(9, 99),
(10, 97),
(11, 97),
(11, 98),
(11, 99),
(11, 100),
(11, 102),
(11, 103),
(11, 104),
(11, 105);

-- --------------------------------------------------------

--
-- Table structure for table `uczelnia_cities`
--

CREATE TABLE `uczelnia_cities` (
  `id` int(11) NOT NULL,
  `name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `fb` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_plus` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `uczelnia_cities`
--

INSERT INTO `uczelnia_cities` (`id`, `name`, `slug`, `color`, `fb`, `google_plus`, `twitter`) VALUES
(97, 'Główna', 'glowna', '008bd2', 'http://www.fb.com', 'https://plus.google.com/?hl=pl', 'https://twitter.com/?lang=pl'),
(98, 'Warszawa', 'warszawa', '7368ab', NULL, 'https://plus.google.com/?hl=pl', 'https://twitter.com/?lang=pl'),
(99, 'Chełm', 'chelm', 'ed5f67', 'http://www.fb.com', 'https://plus.google.com/?hl=pl', 'https://twitter.com/?lang=pl'),
(100, 'Elbląg', 'elblag', 'f99157', 'http://www.fb.com', 'https://plus.google.com/?hl=pl', 'https://twitter.com/?lang=pl'),
(101, 'Kraków', 'krakow', 'fac863', NULL, 'https://plus.google.com/?hl=pl', 'https://twitter.com/?lang=pl'),
(102, 'Łomża', 'lomza', '99c794', 'http://fb.com', 'https://plus.google.com/?hl=pl', 'https://twitter.com/?lang=pl'),
(103, 'Opole', 'opole', '5eb4b3', 'http://www.fb.com', 'https://plus.google.com/?hl=pl', 'https://twitter.com/?lang=pl'),
(104, 'Szczecin', 'szczecin', 'c4c4c4', 'http://www.fb.com', 'https://plus.google.com/?hl=pl', 'https://twitter.com/?lang=pl'),
(105, 'Zabrze', 'zabrze', 'ab7968', 'http://www.fb.com', 'https://plus.google.com/?hl=pl', 'https://twitter.com/?lang=pl');

-- --------------------------------------------------------

--
-- Table structure for table `uczelnia_logs`
--

CREATE TABLE `uczelnia_logs` (
  `id` int(11) NOT NULL,
  `author` int(11) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `uczelnia_logs`
--

INSERT INTO `uczelnia_logs` (`id`, `author`, `create_date`, `message`) VALUES
(1, 30, '2017-01-14 20:10:05', 'Zmieniono Główna: '),
(2, 30, '2017-01-14 20:10:44', 'Zmieniono Główna: kolor[008bd2 -> 008bd2] <br>'),
(3, 30, '2017-01-14 20:11:33', 'Zmieniono Główna: kolor[008bd2 -> 008bd3] '),
(4, 30, '2017-01-14 20:18:14', 'Zmieniono Główna: '),
(5, 30, '2017-01-14 20:18:39', 'Zmieniono Główna: kolor[008bd2 -> 008bd3] <br>Facebook[http://www.fb.com -> http://www.fb.com2] <br>Google+[http://google.com -> http://googlep.com] '),
(6, 30, '2017-01-14 22:34:23', 'Zmieniono Główna: kolor [008bd3 -> 008bd2] <br>Facebook [http://www.fb.com2 -> http://www.fb.com] <br>Google+ [http://googlep.com -> http://google.com] <br>'),
(7, 30, '2017-01-14 23:01:06', 'Zmieniono stronę test 7'),
(8, 30, '2017-01-14 23:01:28', 'Zmieniono stronę test 7 w lomza'),
(9, 30, '2017-01-14 23:02:54', 'Dodano nową stronę Test historii w GłównaWarszawa'),
(10, 30, '2017-01-14 23:04:40', 'Dodano nową stronę Pusta strona - test logów'),
(11, 30, '2017-01-14 23:05:09', 'Zmieniono stronę Test historii w Chełm, Kraków, Łomża'),
(12, 30, '2017-01-14 23:10:22', 'Zmieniono zawartość strony Kontakt w Łomża'),
(13, 30, '2017-01-14 23:15:32', 'Usunięto stronę Test historii z miast: Warszawa Chełm Łomża '),
(14, 30, '2017-01-14 23:19:26', 'Usunięto stronę <i>Test historii</i> z wszystkich miast!'),
(15, 30, '2017-01-14 23:20:11', 'Usunięto stronę <i>Pusta strona - test logów</i> z miast: '),
(16, 30, '2017-01-14 23:51:22', 'Zmieniono stronę <i>Rekrutacja</i> w Główna'),
(17, 30, '2017-01-15 00:30:44', 'Zmieniono Główna: kolor [008bd2 -> 008bd3] <br>'),
(18, 30, '2017-01-15 00:31:22', 'Zmieniono Łomża: Facebook [ -> http://fb.com] <br>Google+ [ -> http://google.com] <br>'),
(19, 30, '2017-01-15 00:32:01', 'Zmieniono Opole: Facebook [http://xyz123 -> http://xyz123.com] <br>'),
(20, 30, '2017-01-15 00:34:36', 'Zmieniono Główna: kolor [008bd3 -> 008bd2] <br>'),
(21, 31, '2017-06-28 22:52:31', 'Zmieniono zawartość strony Kontakt w Warszawa'),
(22, 30, '2017-06-28 22:57:19', 'Dodano nową stronę <i>Dziekanat</i> w Główna'),
(23, 30, '2017-06-28 22:58:51', 'Zmieniono stronę <i>Zarzadzanie</i>'),
(24, 30, '2017-06-28 23:01:22', 'Zmieniono stronę <i>Zasady rekrutacji</i> w Kraków, Warszawa, Chełm, Elbląg, Łomża, Opole, Szczecin, Zabrze'),
(25, 30, '2017-06-28 23:01:58', 'Zmieniono stronę <i>Rekrutacja</i>'),
(26, 30, '2017-06-28 23:02:35', 'Zmieniono stronę <i>Rekrutacja</i> w Warszawa, Chełm, Elbląg, Kraków, Łomża, Opole, Szczecin, Zabrze'),
(27, 30, '2017-06-28 23:03:14', 'Zmieniono stronę <i>Oferta edukacyjna</i> w Warszawa, Chełm, Elbląg, Kraków, Łomża, Opole, Szczecin, Zabrze'),
(28, 30, '2017-06-28 23:03:59', 'Zmieniono stronę <i>Studia I stopnia</i> w Kraków, Warszawa, Chełm, Elbląg, Łomża, Opole, Szczecin, Zabrze'),
(29, 30, '2017-06-28 23:04:33', 'Zmieniono stronę <i>Pedagogika</i> w Kraków'),
(30, 30, '2017-06-28 23:06:01', 'Zmieniono stronę <i>Zarzadzanie</i> w Kraków, Warszawa, Chełm, Elbląg, Łomża, Opole, Szczecin, Zabrze'),
(31, 30, '2017-06-28 23:06:35', 'Zmieniono stronę <i>Pedagogika</i> w Kraków, Warszawa, Chełm, Elbląg, Łomża, Opole, Szczecin, Zabrze'),
(32, 30, '2017-06-28 23:08:05', 'Zmieniono stronę <i>O uczelni</i> w Główna'),
(33, 30, '2017-06-28 23:08:27', 'Zmieniono stronę <i>Projekty</i> w Główna'),
(34, 30, '2017-06-28 23:09:16', 'Dodano nową stronę <i>Wydawnictwo</i> w Główna'),
(35, 30, '2017-06-28 23:11:40', 'Dodano nową stronę <i>Kursy i szkolenia</i> w Główna'),
(36, 30, '2017-06-28 23:12:01', 'Dodano nową stronę <i>Liceum</i> w Główna'),
(37, 30, '2017-06-28 23:12:22', 'Dodano nową stronę <i>Gimnazjum</i>'),
(38, 30, '2017-06-28 23:12:44', 'Zmieniono stronę <i>Gimnazjum</i> w Główna'),
(39, 30, '2017-06-28 23:19:06', 'Zmieniono Zabrze: Facebook [ -> http://www.fb.com] <br>'),
(40, 30, '2017-06-28 23:19:23', 'Zmieniono Szczecin: Facebook [ -> http://www.fb.com] <br>');

-- --------------------------------------------------------

--
-- Table structure for table `uczelnia_news`
--

CREATE TABLE `uczelnia_news` (
  `id` int(11) NOT NULL,
  `title` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `uczelnia_news`
--

INSERT INTO `uczelnia_news` (`id`, `title`, `slug`, `create_date`, `update_date`, `content`, `thumbnail`) VALUES
(9, 'Nowa wiadomość 1', 'nowa-wiadomosc-1', '2012-01-01 12:12:12', '2016-12-07 12:56:18', '<p>fewf frgrg grwg</p>', '/uploads/duzy-obraz'),
(10, 'Nowa wiadomość 2', 'nowa-wiadomosc-2', '2012-01-01 12:12:12', '2016-12-07 12:56:18', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At earum eos molestias nostrum odit veniam vitae! Ab ad alias architecto atque blanditiis consequuntur culpa, cupiditate dignissimos dolor doloremque ea enim error esse facere fugit hic ipsum labore necessitatibus neque nihil nisi obcaecati omnis pariatur porro possimus quae quo quod, quos repellat repellendus similique sint sunt ut veritatis vitae voluptas <strong>voluptate</strong>! Ad assumenda commodi quo reprehenderit!</p>', NULL),
(11, 'Nowość 3', 'nowosc-3', '2016-11-07 18:33:35', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum porttitor, lorem ut consectetur faucibus, turpis velit ornare dui, ac tempus est metus.</p>', '/uploads/news-img.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `uczelnia_pages`
--

CREATE TABLE `uczelnia_pages` (
  `id` int(11) NOT NULL,
  `parent_page_id` int(11) DEFAULT NULL,
  `title` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime DEFAULT NULL,
  `menu_order` int(11) NOT NULL DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `uczelnia_pages`
--

INSERT INTO `uczelnia_pages` (`id`, `parent_page_id`, `title`, `slug`, `create_date`, `update_date`, `menu_order`) VALUES
(115, NULL, 'Kontakt', 'kontakt', '2012-01-01 12:12:12', '2016-12-07 12:56:18', 6),
(116, NULL, 'Oferta edukacyjna', 'oferta-edukacyjna', '2012-01-01 12:12:12', '2016-12-07 12:56:18', 2),
(117, NULL, 'Projekty', 'projekty', '2012-01-01 12:12:12', '2016-12-07 12:56:18', 3),
(118, NULL, 'O uczelni', 'o-uczelni', '2012-01-01 12:12:12', '2016-12-07 12:56:18', 5),
(119, NULL, 'Rekrutacja', 'rekrutacja', '2012-01-01 12:12:12', '2016-12-07 12:56:18', 1),
(120, 119, 'Zasady rekrutacji', 'zasady-rekrutacji', '2012-01-01 12:12:12', '2016-12-07 12:56:18', 1),
(121, 119, 'Rekrutacja Online', 'rekrutacja-online', '2012-01-01 12:12:12', '2016-12-07 12:56:18', 2),
(122, NULL, 'info', 'info', '2012-01-01 12:12:12', '2016-01-01 14:33:09', -1),
(123, NULL, 'Aktualności', 'aktualnosci', '2016-12-08 11:30:47', NULL, 16),
(124, 120, 'test 7', 'test-7', '2016-12-09 18:44:23', NULL, 20),
(125, 116, 'Studia I stopnia', 'studia-i-stopnia', '2016-12-11 14:41:42', NULL, 30),
(126, 125, 'Pedagogika', 'pedagogika', '2016-12-11 14:42:01', NULL, 20),
(127, 116, 'Studia II stopnia', 'studia-ii-stopnia', '2016-12-13 12:13:51', NULL, 10),
(128, 125, 'Zarzadzanie', 'zarzadzanie', '2016-12-13 12:14:20', NULL, 10),
(129, 127, 'infa', 'infa', '2016-12-13 12:14:53', NULL, 0),
(130, 116, 'Studia podyplomowe', 'studia-podyplomowe', '2016-12-15 22:40:20', NULL, 20),
(131, NULL, 'Strefa studenta', 'strefa-studenta', '2016-12-17 20:16:02', NULL, -1),
(132, 131, 'Warszawa', 'warszawa', '2016-12-17 20:16:59', NULL, 9),
(134, NULL, 'Pusta strona - test logów', 'pusta-strona-test-logow', '2017-01-14 23:04:40', NULL, 26),
(135, NULL, 'Dziekanat', 'dziekanat', '2017-06-28 22:57:19', NULL, 36),
(136, NULL, 'Wydawnictwo', 'wydawnictwo', '2017-06-28 23:09:16', NULL, 46),
(137, NULL, 'Kursy i szkolenia', 'kursy-i-szkolenia', '2017-06-28 23:11:40', NULL, 56),
(138, NULL, 'Liceum', 'liceum', '2017-06-28 23:12:01', NULL, 66),
(139, NULL, 'Gimnazjum', 'gimnazjum', '2017-06-28 23:12:22', NULL, 76);

-- --------------------------------------------------------

--
-- Table structure for table `uczelnia_page_contents`
--

CREATE TABLE `uczelnia_page_contents` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `uczelnia_page_contents`
--

INSERT INTO `uczelnia_page_contents` (`id`, `page_id`, `city_id`, `create_date`, `update_date`, `content`) VALUES
(71, 115, 98, '2016-12-07 21:26:11', '2016-12-07 12:56:18', '<h1>Kontakt</h1>\r\n\r\n<p>Informacje kontaktowe dla Warszawy ...</p>'),
(72, 115, 102, '2012-01-01 12:12:12', '2016-12-07 12:56:18', '<p>Informacje kontaktowe dla Łomży...</p>\r\n\r\n<p><strong>Edit:</strong> Edycja dokonana.</p>'),
(73, 120, 102, '2017-06-28 23:01:22', '2016-12-07 12:56:18', '<h1>Zasady rekrutacji</h1>'),
(74, 119, 101, '2017-06-28 23:02:35', NULL, '<h1>Rekrutacja</h1>'),
(75, 120, 101, '2017-06-28 23:01:22', NULL, '<h1>Zasady rekrutacji</h1>'),
(76, 123, 97, '2016-12-08 11:32:20', NULL, '<p>Aktualności</p>'),
(77, 123, 98, '2016-12-08 11:32:20', NULL, '<p>Aktualności</p>'),
(78, 123, 99, '2016-12-08 11:32:20', NULL, '<p>Aktualności</p>'),
(79, 123, 100, '2016-12-08 11:32:20', NULL, '<p>Aktualności</p>'),
(80, 123, 101, '2016-12-08 11:32:20', NULL, '<p>Aktualności</p>'),
(81, 123, 102, '2016-12-08 11:32:20', NULL, '<p>Aktualności</p>'),
(82, 123, 103, '2016-12-08 11:32:20', NULL, '<p>Aktualności</p>'),
(83, 123, 104, '2016-12-08 11:32:20', NULL, '<p>Aktualności</p>'),
(84, 123, 105, '2016-12-08 11:32:20', NULL, '<p>Aktualności</p>'),
(85, 125, 101, '2017-06-28 23:03:59', NULL, '<h1>Studia I stopnia</h1>'),
(86, 126, 101, '2017-06-28 23:06:35', NULL, '<h1>Pedagogika</h1>'),
(87, 116, 101, '2017-06-28 23:03:14', NULL, '<h1>Oferta</h1>'),
(88, 127, 101, '2016-12-13 12:13:51', NULL, '<p>gtytr</p>'),
(89, 128, 101, '2017-06-28 23:06:01', NULL, '<h1>Zarządzanie</h1>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<p>Lorem ipsum.</p>\r\n\r\n<div class="bootstrap-tabs" data-tab-set-title="jk">\r\n<ul class="nav nav-tabs" role="tablist"><!-- add tabs here -->\r\n	<li class="active" role="presentation"><a aria-controls="jk-tab-1" aria-expanded="true" class="tab-link" data-toggle="tab" href="#jk-tab-1" role="tab">Tab 1 Name</a></li>\r\n	<li role="presentation"><a aria-controls="jk-tab-2" class="tab-link" data-toggle="tab" href="#jk-tab-2" role="tab">Tab 2 Name</a></li>\r\n	<li role="presentation"><a aria-controls="jk-tab-3" aria-expanded="false" class="tab-link" data-toggle="tab" href="#jk-tab-3" role="tab">Tab 3 Name</a></li>\r\n</ul>\r\n\r\n<div class="tab-content"><!-- add tab panels here -->\r\n<div class="active tab-pane" id="jk-tab-1" role="tabpanel">\r\n<div class="tab-pane-content">Tab 1 Content</div>\r\n</div>\r\n\r\n<div class="tab-pane" id="jk-tab-2" role="tabpanel">\r\n<div class="tab-pane-content">Tab 2 Content</div>\r\n</div>\r\n\r\n<div class="tab-pane" id="jk-tab-3" role="tabpanel">\r\n<div class="tab-pane-content">Tab 3 Content</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<p>&nbsp;</p>'),
(90, 129, 101, '2016-12-13 12:14:53', NULL, '<p>efrgeehtr</p>'),
(91, 130, 101, '2016-12-15 22:40:20', NULL, '<p>thytrhytjyt</p>'),
(92, 131, 97, '2016-12-17 20:16:02', NULL, '<h1>Strefa studenta</h1>\r\n\r\n<p>Coś na temt strefy studenta.</p>'),
(93, 132, 97, '2016-12-17 20:16:59', NULL, '<h1>Strefa Studenta dla Warszawy</h1>\r\n\r\n<p>bla bla bla...</p>'),
(94, 124, 102, '2017-01-14 23:01:28', NULL, '<p>abc</p>'),
(100, 119, 97, '2017-01-14 23:51:22', NULL, '<p>123</p>'),
(101, 135, 97, '2017-06-28 22:57:19', NULL, '<h1>Dziekanat</h1>'),
(102, 120, 98, '2017-06-28 23:01:22', NULL, '<h1>Zasady rekrutacji</h1>'),
(103, 120, 99, '2017-06-28 23:01:22', NULL, '<h1>Zasady rekrutacji</h1>'),
(104, 120, 100, '2017-06-28 23:01:22', NULL, '<h1>Zasady rekrutacji</h1>'),
(105, 120, 103, '2017-06-28 23:01:22', NULL, '<h1>Zasady rekrutacji</h1>'),
(106, 120, 104, '2017-06-28 23:01:22', NULL, '<h1>Zasady rekrutacji</h1>'),
(107, 120, 105, '2017-06-28 23:01:22', NULL, '<h1>Zasady rekrutacji</h1>'),
(108, 119, 98, '2017-06-28 23:02:35', NULL, '<h1>Rekrutacja</h1>'),
(109, 119, 99, '2017-06-28 23:02:35', NULL, '<h1>Rekrutacja</h1>'),
(110, 119, 100, '2017-06-28 23:02:35', NULL, '<h1>Rekrutacja</h1>'),
(111, 119, 102, '2017-06-28 23:02:35', NULL, '<h1>Rekrutacja</h1>'),
(112, 119, 103, '2017-06-28 23:02:35', NULL, '<h1>Rekrutacja</h1>'),
(113, 119, 104, '2017-06-28 23:02:35', NULL, '<h1>Rekrutacja</h1>'),
(114, 119, 105, '2017-06-28 23:02:35', NULL, '<h1>Rekrutacja</h1>'),
(115, 116, 98, '2017-06-28 23:03:14', NULL, '<h1>Oferta</h1>'),
(116, 116, 99, '2017-06-28 23:03:14', NULL, '<h1>Oferta</h1>'),
(117, 116, 100, '2017-06-28 23:03:14', NULL, '<h1>Oferta</h1>'),
(118, 116, 102, '2017-06-28 23:03:14', NULL, '<h1>Oferta</h1>'),
(119, 116, 103, '2017-06-28 23:03:14', NULL, '<h1>Oferta</h1>'),
(120, 116, 104, '2017-06-28 23:03:14', NULL, '<h1>Oferta</h1>'),
(121, 116, 105, '2017-06-28 23:03:14', NULL, '<h1>Oferta</h1>'),
(122, 125, 98, '2017-06-28 23:03:59', NULL, '<h1>Studia I stopnia</h1>'),
(123, 125, 99, '2017-06-28 23:03:59', NULL, '<h1>Studia I stopnia</h1>'),
(124, 125, 100, '2017-06-28 23:03:59', NULL, '<h1>Studia I stopnia</h1>'),
(125, 125, 102, '2017-06-28 23:03:59', NULL, '<h1>Studia I stopnia</h1>'),
(126, 125, 103, '2017-06-28 23:03:59', NULL, '<h1>Studia I stopnia</h1>'),
(127, 125, 104, '2017-06-28 23:03:59', NULL, '<h1>Studia I stopnia</h1>'),
(128, 125, 105, '2017-06-28 23:03:59', NULL, '<h1>Studia I stopnia</h1>'),
(129, 128, 98, '2017-06-28 23:06:01', NULL, '<h1>Zarządzanie</h1>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<p>Lorem ipsum.</p>\r\n\r\n<div class="bootstrap-tabs" data-tab-set-title="jk">\r\n<ul class="nav nav-tabs" role="tablist"><!-- add tabs here -->\r\n	<li class="active" role="presentation"><a aria-controls="jk-tab-1" aria-expanded="true" class="tab-link" data-toggle="tab" href="#jk-tab-1" role="tab">Tab 1 Name</a></li>\r\n	<li role="presentation"><a aria-controls="jk-tab-2" class="tab-link" data-toggle="tab" href="#jk-tab-2" role="tab">Tab 2 Name</a></li>\r\n	<li role="presentation"><a aria-controls="jk-tab-3" aria-expanded="false" class="tab-link" data-toggle="tab" href="#jk-tab-3" role="tab">Tab 3 Name</a></li>\r\n</ul>\r\n\r\n<div class="tab-content"><!-- add tab panels here -->\r\n<div class="active tab-pane" id="jk-tab-1" role="tabpanel">\r\n<div class="tab-pane-content">Tab 1 Content</div>\r\n</div>\r\n\r\n<div class="tab-pane" id="jk-tab-2" role="tabpanel">\r\n<div class="tab-pane-content">Tab 2 Content</div>\r\n</div>\r\n\r\n<div class="tab-pane" id="jk-tab-3" role="tabpanel">\r\n<div class="tab-pane-content">Tab 3 Content</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<p>&nbsp;</p>'),
(130, 128, 99, '2017-06-28 23:06:01', NULL, '<h1>Zarządzanie</h1>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<p>Lorem ipsum.</p>\r\n\r\n<div class="bootstrap-tabs" data-tab-set-title="jk">\r\n<ul class="nav nav-tabs" role="tablist"><!-- add tabs here -->\r\n	<li class="active" role="presentation"><a aria-controls="jk-tab-1" aria-expanded="true" class="tab-link" data-toggle="tab" href="#jk-tab-1" role="tab">Tab 1 Name</a></li>\r\n	<li role="presentation"><a aria-controls="jk-tab-2" class="tab-link" data-toggle="tab" href="#jk-tab-2" role="tab">Tab 2 Name</a></li>\r\n	<li role="presentation"><a aria-controls="jk-tab-3" aria-expanded="false" class="tab-link" data-toggle="tab" href="#jk-tab-3" role="tab">Tab 3 Name</a></li>\r\n</ul>\r\n\r\n<div class="tab-content"><!-- add tab panels here -->\r\n<div class="active tab-pane" id="jk-tab-1" role="tabpanel">\r\n<div class="tab-pane-content">Tab 1 Content</div>\r\n</div>\r\n\r\n<div class="tab-pane" id="jk-tab-2" role="tabpanel">\r\n<div class="tab-pane-content">Tab 2 Content</div>\r\n</div>\r\n\r\n<div class="tab-pane" id="jk-tab-3" role="tabpanel">\r\n<div class="tab-pane-content">Tab 3 Content</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<p>&nbsp;</p>'),
(131, 128, 100, '2017-06-28 23:06:01', NULL, '<h1>Zarządzanie</h1>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<p>Lorem ipsum.</p>\r\n\r\n<div class="bootstrap-tabs" data-tab-set-title="jk">\r\n<ul class="nav nav-tabs" role="tablist"><!-- add tabs here -->\r\n	<li class="active" role="presentation"><a aria-controls="jk-tab-1" aria-expanded="true" class="tab-link" data-toggle="tab" href="#jk-tab-1" role="tab">Tab 1 Name</a></li>\r\n	<li role="presentation"><a aria-controls="jk-tab-2" class="tab-link" data-toggle="tab" href="#jk-tab-2" role="tab">Tab 2 Name</a></li>\r\n	<li role="presentation"><a aria-controls="jk-tab-3" aria-expanded="false" class="tab-link" data-toggle="tab" href="#jk-tab-3" role="tab">Tab 3 Name</a></li>\r\n</ul>\r\n\r\n<div class="tab-content"><!-- add tab panels here -->\r\n<div class="active tab-pane" id="jk-tab-1" role="tabpanel">\r\n<div class="tab-pane-content">Tab 1 Content</div>\r\n</div>\r\n\r\n<div class="tab-pane" id="jk-tab-2" role="tabpanel">\r\n<div class="tab-pane-content">Tab 2 Content</div>\r\n</div>\r\n\r\n<div class="tab-pane" id="jk-tab-3" role="tabpanel">\r\n<div class="tab-pane-content">Tab 3 Content</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<p>&nbsp;</p>'),
(132, 128, 102, '2017-06-28 23:06:01', NULL, '<h1>Zarządzanie</h1>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<p>Lorem ipsum.</p>\r\n\r\n<div class="bootstrap-tabs" data-tab-set-title="jk">\r\n<ul class="nav nav-tabs" role="tablist"><!-- add tabs here -->\r\n	<li class="active" role="presentation"><a aria-controls="jk-tab-1" aria-expanded="true" class="tab-link" data-toggle="tab" href="#jk-tab-1" role="tab">Tab 1 Name</a></li>\r\n	<li role="presentation"><a aria-controls="jk-tab-2" class="tab-link" data-toggle="tab" href="#jk-tab-2" role="tab">Tab 2 Name</a></li>\r\n	<li role="presentation"><a aria-controls="jk-tab-3" aria-expanded="false" class="tab-link" data-toggle="tab" href="#jk-tab-3" role="tab">Tab 3 Name</a></li>\r\n</ul>\r\n\r\n<div class="tab-content"><!-- add tab panels here -->\r\n<div class="active tab-pane" id="jk-tab-1" role="tabpanel">\r\n<div class="tab-pane-content">Tab 1 Content</div>\r\n</div>\r\n\r\n<div class="tab-pane" id="jk-tab-2" role="tabpanel">\r\n<div class="tab-pane-content">Tab 2 Content</div>\r\n</div>\r\n\r\n<div class="tab-pane" id="jk-tab-3" role="tabpanel">\r\n<div class="tab-pane-content">Tab 3 Content</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<p>&nbsp;</p>'),
(133, 128, 103, '2017-06-28 23:06:01', NULL, '<h1>Zarządzanie</h1>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<p>Lorem ipsum.</p>\r\n\r\n<div class="bootstrap-tabs" data-tab-set-title="jk">\r\n<ul class="nav nav-tabs" role="tablist"><!-- add tabs here -->\r\n	<li class="active" role="presentation"><a aria-controls="jk-tab-1" aria-expanded="true" class="tab-link" data-toggle="tab" href="#jk-tab-1" role="tab">Tab 1 Name</a></li>\r\n	<li role="presentation"><a aria-controls="jk-tab-2" class="tab-link" data-toggle="tab" href="#jk-tab-2" role="tab">Tab 2 Name</a></li>\r\n	<li role="presentation"><a aria-controls="jk-tab-3" aria-expanded="false" class="tab-link" data-toggle="tab" href="#jk-tab-3" role="tab">Tab 3 Name</a></li>\r\n</ul>\r\n\r\n<div class="tab-content"><!-- add tab panels here -->\r\n<div class="active tab-pane" id="jk-tab-1" role="tabpanel">\r\n<div class="tab-pane-content">Tab 1 Content</div>\r\n</div>\r\n\r\n<div class="tab-pane" id="jk-tab-2" role="tabpanel">\r\n<div class="tab-pane-content">Tab 2 Content</div>\r\n</div>\r\n\r\n<div class="tab-pane" id="jk-tab-3" role="tabpanel">\r\n<div class="tab-pane-content">Tab 3 Content</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<p>&nbsp;</p>'),
(134, 128, 104, '2017-06-28 23:06:01', NULL, '<h1>Zarządzanie</h1>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<p>Lorem ipsum.</p>\r\n\r\n<div class="bootstrap-tabs" data-tab-set-title="jk">\r\n<ul class="nav nav-tabs" role="tablist"><!-- add tabs here -->\r\n	<li class="active" role="presentation"><a aria-controls="jk-tab-1" aria-expanded="true" class="tab-link" data-toggle="tab" href="#jk-tab-1" role="tab">Tab 1 Name</a></li>\r\n	<li role="presentation"><a aria-controls="jk-tab-2" class="tab-link" data-toggle="tab" href="#jk-tab-2" role="tab">Tab 2 Name</a></li>\r\n	<li role="presentation"><a aria-controls="jk-tab-3" aria-expanded="false" class="tab-link" data-toggle="tab" href="#jk-tab-3" role="tab">Tab 3 Name</a></li>\r\n</ul>\r\n\r\n<div class="tab-content"><!-- add tab panels here -->\r\n<div class="active tab-pane" id="jk-tab-1" role="tabpanel">\r\n<div class="tab-pane-content">Tab 1 Content</div>\r\n</div>\r\n\r\n<div class="tab-pane" id="jk-tab-2" role="tabpanel">\r\n<div class="tab-pane-content">Tab 2 Content</div>\r\n</div>\r\n\r\n<div class="tab-pane" id="jk-tab-3" role="tabpanel">\r\n<div class="tab-pane-content">Tab 3 Content</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<p>&nbsp;</p>'),
(135, 128, 105, '2017-06-28 23:06:01', NULL, '<h1>Zarządzanie</h1>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<p>Lorem ipsum.</p>\r\n\r\n<div class="bootstrap-tabs" data-tab-set-title="jk">\r\n<ul class="nav nav-tabs" role="tablist"><!-- add tabs here -->\r\n	<li class="active" role="presentation"><a aria-controls="jk-tab-1" aria-expanded="true" class="tab-link" data-toggle="tab" href="#jk-tab-1" role="tab">Tab 1 Name</a></li>\r\n	<li role="presentation"><a aria-controls="jk-tab-2" class="tab-link" data-toggle="tab" href="#jk-tab-2" role="tab">Tab 2 Name</a></li>\r\n	<li role="presentation"><a aria-controls="jk-tab-3" aria-expanded="false" class="tab-link" data-toggle="tab" href="#jk-tab-3" role="tab">Tab 3 Name</a></li>\r\n</ul>\r\n\r\n<div class="tab-content"><!-- add tab panels here -->\r\n<div class="active tab-pane" id="jk-tab-1" role="tabpanel">\r\n<div class="tab-pane-content">Tab 1 Content</div>\r\n</div>\r\n\r\n<div class="tab-pane" id="jk-tab-2" role="tabpanel">\r\n<div class="tab-pane-content">Tab 2 Content</div>\r\n</div>\r\n\r\n<div class="tab-pane" id="jk-tab-3" role="tabpanel">\r\n<div class="tab-pane-content">Tab 3 Content</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<p>&nbsp;</p>'),
(136, 126, 98, '2017-06-28 23:06:35', NULL, '<h1>Pedagogika</h1>'),
(137, 126, 99, '2017-06-28 23:06:35', NULL, '<h1>Pedagogika</h1>'),
(138, 126, 100, '2017-06-28 23:06:35', NULL, '<h1>Pedagogika</h1>'),
(139, 126, 102, '2017-06-28 23:06:35', NULL, '<h1>Pedagogika</h1>'),
(140, 126, 103, '2017-06-28 23:06:35', NULL, '<h1>Pedagogika</h1>'),
(141, 126, 104, '2017-06-28 23:06:35', NULL, '<h1>Pedagogika</h1>'),
(142, 126, 105, '2017-06-28 23:06:35', NULL, '<h1>Pedagogika</h1>'),
(143, 118, 97, '2017-06-28 23:08:05', NULL, '<h1>O uczelni</h1>'),
(144, 117, 97, '2017-06-28 23:08:27', NULL, '<h1>Projekty</h1>'),
(145, 136, 97, '2017-06-28 23:09:16', NULL, '<h1>Wydawnictwo</h1>'),
(146, 137, 97, '2017-06-28 23:11:40', NULL, '<h1>Kursy i szkolenia</h1>'),
(147, 138, 97, '2017-06-28 23:12:01', NULL, '<h1>Liceum</h1>'),
(148, 139, 97, '2017-06-28 23:12:44', NULL, '<h1>Gimnazjum</h1>');

-- --------------------------------------------------------

--
-- Table structure for table `uczelnia_slider`
--

CREATE TABLE `uczelnia_slider` (
  `id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime DEFAULT NULL,
  `slide_img` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `slide_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `uczelnia_slider`
--

INSERT INTO `uczelnia_slider` (`id`, `create_date`, `update_date`, `slide_img`, `slide_order`) VALUES
(5, '2016-12-09 00:07:09', NULL, '/uploads/slide1.png', 21),
(6, '2016-12-09 00:14:38', NULL, '/uploads/slide2.png', 31);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `account_non_expired` tinyint(1) NOT NULL,
  `account_non_locked` tinyint(1) NOT NULL,
  `credentials_non_expired` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `action_token` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `register_date` datetime NOT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `account_non_expired`, `account_non_locked`, `credentials_non_expired`, `enabled`, `roles`, `action_token`, `register_date`, `update_date`) VALUES
(30, 'mar3', '$2y$13$k4xf7OX9GLBfdQQHwOHwIOYuXjH5u.GABzJaHQOuCkhbed1u4sTtG', 1, 1, 1, 1, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', NULL, '2016-12-07 12:56:17', '2017-06-29 12:07:08'),
(31, 'nowy1', '$2y$13$1RP1CHWpbQphLujWT8x7G.tgjoyOhGH4o8127yW9slKXXX4tW0/UG', 1, 1, 1, 1, 'a:1:{i:0;s:11:"ROLE_EDITOR";}', NULL, '2016-12-09 21:09:05', '2017-06-28 22:51:52'),
(32, 'nowy2', '$2y$13$OmF10xYXXn2K6mueXgOtI.JoBX39S.ssbwC2KQApR/7ycnBGteyFa', 0, 1, 1, 1, 'a:1:{i:0;s:11:"ROLE_EDITOR";}', 'd4c99b8a735d567a2efc', '2016-12-10 14:30:17', '2016-12-11 12:56:27'),
(33, 'nowy3', '$2y$13$5uJXLG8YtvVwJCqrKznyF.iI5egbOWdJ454.FYQd2JuHnYYJ7l/Ia', 1, 1, 1, 1, 'a:1:{i:0;s:11:"ROLE_EDITOR";}', NULL, '2016-12-10 15:24:17', '2016-12-10 15:24:17');

-- --------------------------------------------------------

--
-- Table structure for table `user_city`
--

CREATE TABLE `user_city` (
  `user_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_city`
--

INSERT INTO `user_city` (`user_id`, `city_id`) VALUES
(31, 98),
(31, 99),
(32, 101),
(33, 98),
(33, 100);

-- --------------------------------------------------------

--
-- Table structure for table `user_page`
--

CREATE TABLE `user_page` (
  `user_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_page`
--

INSERT INTO `user_page` (`user_id`, `page_id`) VALUES
(31, 115),
(31, 118),
(32, 115),
(32, 119),
(32, 120),
(32, 121),
(32, 124),
(33, 115),
(33, 117),
(33, 118);

-- --------------------------------------------------------

--
-- Table structure for table `user_passwords`
--

CREATE TABLE `user_passwords` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_passwords`
--

INSERT INTO `user_passwords` (`id`, `user_id`, `password`) VALUES
(4, 32, '$2y$13$Rf4R7.1u7W/mF3wpeB9xUOzr0zj6UCnIBIpbnVs1ZET/3CnBzVd1O'),
(6, 32, '$2y$13$Mw3gbX2J/pJVhT6KGKmGp.AgiroGxmiJVruvCwAa0ZByOD2hIVKze'),
(7, 31, '$2y$13$0jElRx7Tmq5e/xu4TQIcOOg9xkDUXoNBM10Sbbi8CZwk90o0OPfou'),
(8, 30, '$2y$13$xtxaZFxnaQgz2sEMgQ346./gNCEGfF5IRRmGRr0RRwu64s0vHfxGy'),
(9, 30, '$2y$13$k4xf7OX9GLBfdQQHwOHwIOYuXjH5u.GABzJaHQOuCkhbed1u4sTtG');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `news_city`
--
ALTER TABLE `news_city`
  ADD PRIMARY KEY (`news_id`,`city_id`),
  ADD KEY `IDX_F886F4BFB5A459A0` (`news_id`),
  ADD KEY `IDX_F886F4BF8BAC62AF` (`city_id`);

--
-- Indexes for table `uczelnia_cities`
--
ALTER TABLE `uczelnia_cities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_3B6374475E237E06` (`name`),
  ADD UNIQUE KEY `UNIQ_3B637447989D9B62` (`slug`);

--
-- Indexes for table `uczelnia_logs`
--
ALTER TABLE `uczelnia_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9C65DB8CBDAFD8C8` (`author`);

--
-- Indexes for table `uczelnia_news`
--
ALTER TABLE `uczelnia_news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_713984802B36786B` (`title`),
  ADD UNIQUE KEY `UNIQ_71398480989D9B62` (`slug`);

--
-- Indexes for table `uczelnia_pages`
--
ALTER TABLE `uczelnia_pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_A6CBDDBC2B36786B` (`title`),
  ADD UNIQUE KEY `UNIQ_A6CBDDBC989D9B62` (`slug`),
  ADD KEY `IDX_A6CBDDBC7E0E17A2` (`parent_page_id`);

--
-- Indexes for table `uczelnia_page_contents`
--
ALTER TABLE `uczelnia_page_contents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1F3A45ECC4663E4` (`page_id`),
  ADD KEY `IDX_1F3A45EC8BAC62AF` (`city_id`);

--
-- Indexes for table `uczelnia_slider`
--
ALTER TABLE `uczelnia_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1483A5E9F85E0677` (`username`);

--
-- Indexes for table `user_city`
--
ALTER TABLE `user_city`
  ADD PRIMARY KEY (`user_id`,`city_id`),
  ADD KEY `IDX_57DA4EFDA76ED395` (`user_id`),
  ADD KEY `IDX_57DA4EFD8BAC62AF` (`city_id`);

--
-- Indexes for table `user_page`
--
ALTER TABLE `user_page`
  ADD PRIMARY KEY (`user_id`,`page_id`),
  ADD KEY `IDX_6E8BFAE9A76ED395` (`user_id`),
  ADD KEY `IDX_6E8BFAE9C4663E4` (`page_id`);

--
-- Indexes for table `user_passwords`
--
ALTER TABLE `user_passwords`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_ED62A6F2A76ED395` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `uczelnia_cities`
--
ALTER TABLE `uczelnia_cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `uczelnia_logs`
--
ALTER TABLE `uczelnia_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `uczelnia_news`
--
ALTER TABLE `uczelnia_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `uczelnia_pages`
--
ALTER TABLE `uczelnia_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;
--
-- AUTO_INCREMENT for table `uczelnia_page_contents`
--
ALTER TABLE `uczelnia_page_contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;
--
-- AUTO_INCREMENT for table `uczelnia_slider`
--
ALTER TABLE `uczelnia_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `user_passwords`
--
ALTER TABLE `user_passwords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `news_city`
--
ALTER TABLE `news_city`
  ADD CONSTRAINT `FK_F886F4BF8BAC62AF` FOREIGN KEY (`city_id`) REFERENCES `uczelnia_cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_F886F4BFB5A459A0` FOREIGN KEY (`news_id`) REFERENCES `uczelnia_news` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `uczelnia_logs`
--
ALTER TABLE `uczelnia_logs`
  ADD CONSTRAINT `FK_9C65DB8CBDAFD8C8` FOREIGN KEY (`author`) REFERENCES `users` (`id`);

--
-- Constraints for table `uczelnia_pages`
--
ALTER TABLE `uczelnia_pages`
  ADD CONSTRAINT `FK_A6CBDDBC7E0E17A2` FOREIGN KEY (`parent_page_id`) REFERENCES `uczelnia_pages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `uczelnia_page_contents`
--
ALTER TABLE `uczelnia_page_contents`
  ADD CONSTRAINT `FK_1F3A45EC8BAC62AF` FOREIGN KEY (`city_id`) REFERENCES `uczelnia_cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_1F3A45ECC4663E4` FOREIGN KEY (`page_id`) REFERENCES `uczelnia_pages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_city`
--
ALTER TABLE `user_city`
  ADD CONSTRAINT `FK_57DA4EFD8BAC62AF` FOREIGN KEY (`city_id`) REFERENCES `uczelnia_cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_57DA4EFDA76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_page`
--
ALTER TABLE `user_page`
  ADD CONSTRAINT `FK_6E8BFAE9A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_6E8BFAE9C4663E4` FOREIGN KEY (`page_id`) REFERENCES `uczelnia_pages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_passwords`
--
ALTER TABLE `user_passwords`
  ADD CONSTRAINT `FK_ED62A6F2A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
